package com.xiaomaotongzhi.randomBuy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ParameterType;
import springfox.documentation.service.RequestParameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xiaomaotongzhi.randomBuy.controller"))
                .paths(PathSelectors.any())
                .build().apiInfo(new ApiInfoBuilder().title("随手买")
                        .description("接口文档")
                        .version("1.0")
                        .contact(new Contact("毛群祥","www.xiaomaotongzhi.com","2359643054@qq.com"))
                        .build())
                .globalRequestParameters(globalRequestParameters());
        
    }

    private List<RequestParameter> globalRequestParameters() {
        List<RequestParameter> params = new ArrayList<>();

        // Add a request parameter for the global header
        params.add(new RequestParameterBuilder()
                .name("Authorization")
                .description("Authorization token")
                .in(ParameterType.HEADER)
                .required(true)
                .query(param -> param.model(model -> model.scalarModel(ScalarType.STRING)))
                .build());

        return params;
    }
}
