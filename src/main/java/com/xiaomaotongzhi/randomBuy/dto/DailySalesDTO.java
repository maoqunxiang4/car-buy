package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DailySalesDTO {
    private String productName ;
    private String driverName ;
    private LocalDateTime orderDate ;
    private double amount ;
    private double price ;
}
