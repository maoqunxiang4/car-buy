package com.xiaomaotongzhi.randomBuy.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class SupplyStationProductInfoDTO {
    private String productName;          // 产品名称
    private String description;          // 产品描述
    private Double price;                // 产品价格
    private String imageUrl;             // 产品图片URL
    private String category;             // 产品类别
    private String location;      // 补货站位置
    private Long stockQuantity;       // 存货数量
    private LocalDateTime lastUpdated;       // 最后更新时间
}
