package com.xiaomaotongzhi.randomBuy.dto;

import lombok.Data;

@Data
public class AdPlaybackStatisticsDTO {
    private DailyDriverAdPlaybackRecordDTO dailyPlaybacks;
    private AdTotalPlaybackRecordDTO totalPlaybackRecord;
    private double dailyTotalViews;
    private double totalViews;

    public AdPlaybackStatisticsDTO(DailyDriverAdPlaybackRecordDTO dailyPlaybacks
            , AdTotalPlaybackRecordDTO totalPlaybackRecord) {
        this.dailyPlaybacks = dailyPlaybacks;
        this.totalPlaybackRecord = totalPlaybackRecord;
        this.dailyTotalViews = dailyPlaybacks.getViews();
        this.totalViews = totalPlaybackRecord.getViews();
    }
}

