package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SupplyStationLocationDTO {
    private String location ;
    private String x ;
    private String y ;
}
