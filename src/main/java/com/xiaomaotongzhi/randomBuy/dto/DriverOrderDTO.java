package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DriverOrderDTO {
    private String productName ;
    private String passengerName ;
    private String driverName ;
    private String orderDate ;
    private double amount ;
    private String vehicleId ;
}
