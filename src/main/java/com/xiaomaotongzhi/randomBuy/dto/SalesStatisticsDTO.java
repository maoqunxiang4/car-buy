package com.xiaomaotongzhi.randomBuy.dto;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.SupplyStationDailySalesRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.SupplyStationTotalSalesRecord;
import com.xiaomaotongzhi.randomBuy.mapper.SupplyStationDailySalesRecordMapper;
import com.xiaomaotongzhi.randomBuy.mapper.SupplyStationTotalSalesRecordMapper;
import com.xiaomaotongzhi.randomBuy.utils.MapperFactory;

import java.util.List;

public class SalesStatisticsDTO {
    private MapperFactory mapperFactory = MapperFactory.getInstance() ;
    private List<SupplyStationDailySalesRecord> dailySalesRecords;
    private List<SupplyStationTotalSalesRecord> totalSalesRecords;
    private double dailyTotalRevenue;
    private double totalRevenue;
    private double dailyTotalProducts;
    private double totalProducts;

    private String stationId ;
    // 构造函数
    public SalesStatisticsDTO() {
        SupplyStationDailySalesRecordMapper supplyStationDailySalesRecordMapper =
                (SupplyStationDailySalesRecordMapper) mapperFactory.getToolHandler("supplyStationDailySalesRecordMapper");
        SupplyStationTotalSalesRecordMapper supplyStationTotalSalesRecordMapper =
                (SupplyStationTotalSalesRecordMapper) mapperFactory.getToolHandler("supplyStationTotalSalesRecordMapper");

        this.dailySalesRecords = supplyStationDailySalesRecordMapper.selectList(
                new QueryWrapper<SupplyStationDailySalesRecord>().eq("id", stationId)
        );

        // 获取总体销售记录
        this.totalSalesRecords = supplyStationTotalSalesRecordMapper.selectList(
                new QueryWrapper<SupplyStationTotalSalesRecord>().eq("station_id", stationId)
        );

        this.dailyTotalRevenue = dailySalesRecords.stream()
                .mapToDouble(SupplyStationDailySalesRecord::getDailyRevenue)
                .sum();
        this.totalRevenue = totalSalesRecords.stream()
                .mapToDouble(SupplyStationTotalSalesRecord::getTotalRevenue)
                .sum();
        this.dailyTotalProducts = dailySalesRecords.stream()
                .mapToDouble(SupplyStationDailySalesRecord::getProductValue)
                .sum();
        this.totalProducts = totalSalesRecords.stream()
                .mapToDouble(SupplyStationTotalSalesRecord::getTotalNumber)
                .sum();
    }

        // Getter 和 Setter 方法
    public List<SupplyStationDailySalesRecord> getDailySalesRecords() {
        return dailySalesRecords;
    }

    public void setDailySalesRecords(List<SupplyStationDailySalesRecord> dailySalesRecords) {
        this.dailySalesRecords = dailySalesRecords;
    }

    public List<SupplyStationTotalSalesRecord> getTotalSalesRecords() {
        return totalSalesRecords;
    }

    public void setTotalSalesRecords(List<SupplyStationTotalSalesRecord> totalSalesRecords) {
        this.totalSalesRecords = totalSalesRecords;
    }

    public double getDailyTotalRevenue() {
        return dailyTotalRevenue;
    }

    public void setDailyTotalRevenue(double dailyTotalRevenue) {
        this.dailyTotalRevenue = dailyTotalRevenue;
    }

    public double getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(double totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public double getDailyTotalProducts() {
        return dailyTotalProducts;
    }

    public void setDailyTotalProducts(double dailyTotalProducts) {
        this.dailyTotalProducts = dailyTotalProducts;
    }

    public double getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(double totalProducts) {
        this.totalProducts = totalProducts;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }
}

