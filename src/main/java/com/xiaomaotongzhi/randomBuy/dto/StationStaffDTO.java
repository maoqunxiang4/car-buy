package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StationStaffDTO {
    private String username ;
    private String mobile ;
    private String email ;
    private String address ;
    private String stationName;
    private String role;
}
