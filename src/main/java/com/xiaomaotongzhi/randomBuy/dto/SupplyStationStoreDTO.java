package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SupplyStationStoreDTO {
    private String productName ;
    private String description ;
    private Double price ;
    private Long stock ;
    private String imageUrl ;
    private String category ;
}
