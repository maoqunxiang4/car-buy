package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PassengerDTO {
    protected String id ;
    protected String mobile ;
}
