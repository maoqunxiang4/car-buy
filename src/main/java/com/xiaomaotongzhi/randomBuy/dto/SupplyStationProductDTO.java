package com.xiaomaotongzhi.randomBuy.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SupplyStationProductDTO {
    private String productName;          // 产品名称
    private String description;          // 产品描述
    private Double price;                // 产品价格
    private String imageUrl;             // 产品图片URL
    private String category;             // 产品类别
}
