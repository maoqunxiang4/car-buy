package com.xiaomaotongzhi.randomBuy.utils;

public interface MapKey {
    public static final String PRODUCT_ID = "product_id" ;
    public static final String PASSENGER_ID = "passenger_id" ;
    public static final String DRIVER_ID = "driver_id" ;
    public static final String ORDER_DATE = "order_date" ;
    public static final String AMOUNT = "amount" ;
    public static final String VEHICLE_ID = "vehicle_id" ;
    public static final String ORDER_PRICE = "order_price" ;
    public static final String SALE_PRICE = "sale_price" ;
    public static final String STATION_ID = "station_id" ;
    public static final String X = "x" ;
    public static final String Y = "y" ;
    public static final String SALES_RECORD_ID = "sales_record_id" ;
    public static final String USER_BEHAVIOR_RECORD_ID = "user_behavior_record_id" ;
    public static final String PURCHASE_OVER = "purchase" ;
    public static final String PURCHASE_UNOVER = "purchase_unover" ;
    public static final String ADVERTISEMENT_TOTAL_ID = "ad_total_id" ;
    public static final String ADVERTISEMENT_DRIVER_DAILY_ID = "ad_driver_daily_id" ;
    public static final String ADVERTISEMENT_ID = "ad_id" ;
    public static final String ADVERTISEMENT_TITLE = "ad_title" ;
    public static final String ADVERTISEMENT_CONTENT = "ad_content" ;
    public static final String ADVERTISEMENT_VERTISER = "ad_vertiser" ;
    public static final String ADVERTISEMENT_STARTDATE = "ad_startDate" ;
    public static final String ADVERTISEMENT_ENDDATE = "ad_endDate" ;
    public static final String ADVERTISEMENT_TARGETAUDIENCE = "ad_targetAudience" ;

}
