package com.xiaomaotongzhi.randomBuy.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

@Component
public class ObjectFactory implements ApplicationContextAware , InitializingBean {

    private Map<String , ObjectBean > handlerMap ;
    private ApplicationContext applicationContext ;

    @Override
    public void afterPropertiesSet() throws Exception {
        final Map<String , ObjectBean> handlerMap = this.applicationContext.getBeansOfType(ObjectBean.class) ;
        this.handlerMap = handlerMap ;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext ;
    }

    public ObjectBean getObjectBean(String key) {
        return handlerMap.get(key) ;
    }
}
