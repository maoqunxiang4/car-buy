package com.xiaomaotongzhi.randomBuy.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.Map;

//@Service("mapperBeanFactory")
@Service
public class MapperFactory implements ApplicationContextAware , InitializingBean {

    private static Map<String , MapperBean> handlerMap ;
    private static ApplicationContext applicationContext ;

    private MapperFactory() {

    }

    @Override
    public void afterPropertiesSet() throws Exception {
        final Map<String , MapperBean> handlerMap = applicationContext.getBeansOfType(MapperBean.class);
        this.handlerMap = handlerMap ;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        MapperFactory.applicationContext = applicationContext ;
    }

    public MapperBean getToolHandler(String BeanName) {
        return handlerMap.get(BeanName) ;
    }

    private static final class MapperFactoryHolder {
        private static final MapperFactory mapperFactory = new MapperFactory() ;
    }

    public static final MapperFactory getInstance() {
        return MapperFactoryHolder.mapperFactory ;
    }

}