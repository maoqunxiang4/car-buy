package com.xiaomaotongzhi.randomBuy.utils;

public enum StatusEnum {
    ACTIVE("ACTIVITY"),
    UN_ACTIVITY("UN_ACTIVITY") ;

    public final String status ;

    StatusEnum(String status) {
        this.status = status ;
    }

}
