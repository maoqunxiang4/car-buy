package com.xiaomaotongzhi.randomBuy.utils;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LocationUtils {

    @Value("${geo.key}")
    private static String key ;

    public static Location getLocation() {
        String jsonStr = HttpUtil.get("https://restapi.amap.com/v3/ip?parameters&key=f8c99db7b8101abbb21f92b10d5eb08c");
        System.out.println(jsonStr);
        return JSONUtil.toBean(jsonStr, Location.class);
    }

    public static class Location {
        private int status ;
        private String  info ;
        private String infocode ;
        private String province ;
        private String city ;
        private String adcode ;
        private String rectangle ;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getInfocode() {
            return infocode;
        }

        public void setInfocode(String infocode) {
            this.infocode = infocode;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAdcode() {
            return adcode;
        }

        public void setAdcode(String adcode) {
            this.adcode = adcode;
        }

        public String getRectangle() {
            return rectangle;
        }

        public void setRectangle(String rectangle) {
            this.rectangle = rectangle;
        }
    }
}
