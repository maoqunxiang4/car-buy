package com.xiaomaotongzhi.randomBuy.utils;

import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Product;
import com.xiaomaotongzhi.randomBuy.mapper.ProductMapper;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsualUtils {
    private static final MapperFactory mapperFactory = MapperFactory.getInstance() ;

    public static String convertLocalDateTime(LocalDateTime localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss") ;
        return dateTimeFormatter.format(localDateTime) ;
    }

    public static String getTimeNowStr() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTimeFormatter.format(getTimeNow()) ;
    }

    public static LocalDateTime getTimeNow() {
        // 1. 获取当前的 LocalDateTime
        LocalDateTime now = LocalDateTime.now();

        // 2. 获取当前时区和目标时区
        ZoneId currentZone = ZoneId.systemDefault();
        ZoneId beijingZone = ZoneId.of("Asia/Shanghai");

        // 3. 将 LocalDateTime 转换为 ZonedDateTime
        ZonedDateTime currentDateTime = now.atZone(currentZone);

        // 4. 将当前时区的时间转换为北京时区的时间
        ZonedDateTime beijingDateTime = currentDateTime.withZoneSameInstant(beijingZone);

        return beijingDateTime.toLocalDateTime();
    }

    public static List<String> getCategoryListByIds(List<String> productIdList) {
        ProductMapper productMapper = (ProductMapper) mapperFactory.getToolHandler("supplyStationProductMapper");
        ArrayList<String> categoryList = new ArrayList<>() ;
        List<Product> products = productMapper.selectBatchIds(productIdList);
        products.stream().forEach(supplyStationProduct -> {
            categoryList.add(supplyStationProduct.getCategory()) ;
        });
        return categoryList;
    }

    public static List<String> getCategoryListByProducts(List<DriverProduct> driverProductList) {
        ArrayList<String> categoryList = new ArrayList<>() ;
        for (DriverProduct driverProduct : driverProductList) {
            categoryList.add(driverProduct.getCategory()) ;
        }
        return categoryList.stream().distinct().collect(Collectors.toList());
    }
}
