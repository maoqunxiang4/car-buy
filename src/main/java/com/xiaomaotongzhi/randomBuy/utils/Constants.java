package com.xiaomaotongzhi.randomBuy.utils;

public interface Constants {
    public static final String SUPPLYSTATION_ENTITY = "SupplyStation" ;
    public static final String ADVERTISEMENT_ENTITY = "Advertisement" ;
    public static final String DEVICE_ENTITY = "Device" ;
    public static final String ORDER_ENTITY = "Order" ;
    public static final String PRODUCT_ENTITY = "Product" ;
    public static final Integer VALUE_UNIT = 10000 ;
    public static final String DATE_FORMATTER = "yyyy-MM-dd HH-mm-ss" ;
    public static final String UNUSE = "未使用" ;
    public static final String USE = "已使用使用" ;
    public static final String USER_TOKEN_PREFIX = "user:token:" ;
    public static final String AUTHORIZATION = "Authorization" ;
    public static final Integer USER_TOKEN_EXPIRE_TIME = 30 ;
    public static final String PASSENGER = "passenger" ;
    public static final String DRIVER = "driver" ;
    public static final String STATIONSTAFF = "stationStaff" ;
    public static final String AC_STATUS = "ACTIVITE" ;
    public static final String UN_STATUS = "UN_ACTIVITE" ;
}
