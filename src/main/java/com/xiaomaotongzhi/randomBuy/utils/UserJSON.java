package com.xiaomaotongzhi.randomBuy.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserJSON {
    private String openid ;
    private String userName ;
    private String session_key ;
    private String unionid ;
    private String errcode ;
    private String errmsg ;
    private String jwtOpenId ;
    private String role ;
}
