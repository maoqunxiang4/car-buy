package com.xiaomaotongzhi.randomBuy.utils;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class ParamsFactory {
    public static final HashMap<String , Object> paramsMap = new HashMap<String ,Object>() ;

    public static Object getParams(String key) {
        Object obj = paramsMap.get(key);
        paramsMap.remove(key) ;
        return obj ;
    }

    public static Object setParams(String key ,Object value) {
        return paramsMap.put(key,value) ;
    }

    public static List<Object> getParamsListByOrder(String... keys) {
        List<Object> list = new ArrayList<Object>() ;
        for (String key : keys) {
            list.add(getParams(key));
        }
        return list;
    }

    public static <clazz> List<clazz> getSpecifyParamsListByOrder(Class<?> clazz , String... keys) {
        ArrayList<clazz> list = new ArrayList<clazz>();
        for (String key : keys) {
            assert clazz != null;
            list.add((clazz)getParams(key)) ;
        }
        return list ;
    }
}