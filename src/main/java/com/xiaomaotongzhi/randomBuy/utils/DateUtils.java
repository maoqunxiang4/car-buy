package com.xiaomaotongzhi.randomBuy.utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;

public class DateUtils {

    private static final ZoneId CHINA_ZONE_ID = ZoneId.of("Asia/Shanghai");

    /**
     * 获取上一周的周一的日期（基于中国时区）
     * @return 上一周的周一的日期
     */
    public static LocalDate getLastWeekMonday() {
        return ZonedDateTime.now(CHINA_ZONE_ID)
                .minusWeeks(1)  // 移到上一周
                .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY))  // 找到那周的周一
                .toLocalDate();
    }

    /**
     * 获取上一周的周日的日期（基于中国时区）
     * @return 上一周的周日的日期
     */
    public static LocalDate getLastWeekSunday() {
        return ZonedDateTime.now(CHINA_ZONE_ID)
                .minusWeeks(1)  // 移到上一周
                .with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY)).plusDays(6)  // 找到那周的周一
                .toLocalDate();
    }
}


