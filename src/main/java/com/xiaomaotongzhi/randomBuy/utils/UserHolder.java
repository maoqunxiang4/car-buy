package com.xiaomaotongzhi.randomBuy.utils;

import com.xiaomaotongzhi.randomBuy.entity.Person.Person;

public class UserHolder {
    public static ThreadLocal<UserJSON> threadLocal = new ThreadLocal<UserJSON>() ;

    public static UserJSON getPerson() {
        return threadLocal.get() ;
    }

    public static void setPerson(UserJSON userJSON) {
        threadLocal.set(userJSON);
    }

    public static void removePerson() {
        threadLocal.remove();
    }
}
