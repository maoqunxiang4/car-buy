package com.xiaomaotongzhi.randomBuy.utils;

import cn.hutool.json.JSONUtil;
import lombok.Data;

import java.util.Objects;

@Data
public class Result {
    private Integer code ;
    private Object data ;
    private String message ;

    public Result(Integer code, Object data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public static Result success(Integer code , Object data ) {
        return new Result(code, data, null) ;
    }

    public static Result success(Integer code ) {
        return new Result(code, null, null) ;
    }

    public static Result fail(Integer code , String message) {
        return new Result(code, null, message) ;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
