package com.xiaomaotongzhi.randomBuy.utils;

import cn.hutool.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LocationTool {

    private static final String IP_LOCATION_API = "http://ip-api.com/json/";

    public static double[] getCoordinates() {
        double[] coordinates = new double[2];
        try {
            URL url = new URL(IP_LOCATION_API);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            connection.disconnect();

            JSONObject jsonObject = new JSONObject(content.toString());
            coordinates[0] = jsonObject.getDouble("lat");
            coordinates[1] = jsonObject.getDouble("lon");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return coordinates;
    }
}