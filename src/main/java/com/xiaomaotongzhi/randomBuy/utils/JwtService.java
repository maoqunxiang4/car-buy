package com.xiaomaotongzhi.randomBuy.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;

public class JwtService {
    private static final String SECRET_KEY = "yourSecretKey"; // 请确保这个密钥是安全的
    private static final long EXPIRATION_TIME = 3600000; // 1 hour

    public String generateToken(String openid) {
        return Jwts.builder()
                .setSubject(openid)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY)
                .compact();
    }

    public Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }
}

