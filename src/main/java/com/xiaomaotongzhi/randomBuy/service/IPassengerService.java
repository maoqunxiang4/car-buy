package com.xiaomaotongzhi.randomBuy.service;

import com.xiaomaotongzhi.randomBuy.utils.Result;

public interface IPassengerService {
    //登陆
    Result login(String code) ;
    //申请身份
    Result verifyRole(String verify, String stationId, String role, String vehicleId);
    // 获取特定用户的优惠券
    Result getUserCoupons();
    // 用户领取优惠券
    Result claimCoupon(String couponId);
    // 使用优惠券
    Result useCoupon(String couponId);
    // 查看优惠券详情
    Result getCouponDetail(String couponId);
    // 获取所有的优惠券
    Result getAllCoupons();
    // 用户查看个人信息
    Result getUserProfile();
    // 用户查看历史订单
    Result getUserOrderHistory();
    // 用户查看当前订单
    Result getUserCurrentOrder();
}
