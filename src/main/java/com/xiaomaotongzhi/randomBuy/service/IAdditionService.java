package com.xiaomaotongzhi.randomBuy.service;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Advertisement;
import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.entity.Person.Driver;
import com.xiaomaotongzhi.randomBuy.utils.Result;

public interface IAdditionService {
    Result listProducts(String driverId ,int page ,int size) ;
    Result getProductDetails(String productId) ;
    Result buyProduct(DriverProduct driverProduct, Double price , Integer amount , Driver driver , String x , String y) ;
    Result listUserOrders(String userId) ;
    Result getOrderDetails(String orderId) ;
    Result addProduct(DriverProduct driverProductInfo) ;
    Result updateProduct(DriverProduct driverProductInfo) ;
    Result deleteProduct(String productId) ;
    Result listAdsForDriver(String driverId) ;
    Result watchAdvertisement(String adId, String driverId) ;
//    Result recordAdPlayback(String adId, String driverId) ;
    Result addAd(Advertisement adInfo) ;
    Result updateAd(Advertisement adInfo) ;
    Result deleteAd(String adId) ;
//    Result listAdPlaybacks(String adId) ;
    Result getAdPlaybackStatistics(String driverId) ;
}
