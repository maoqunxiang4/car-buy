package com.xiaomaotongzhi.randomBuy.service;

import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.entity.Addition.SupplyStation;
import com.xiaomaotongzhi.randomBuy.entity.Addition.SupplyStationStore;
import com.xiaomaotongzhi.randomBuy.entity.Person.StationStaff;
import com.xiaomaotongzhi.randomBuy.utils.Result;

import java.awt.*;
import java.util.List;

public interface ISupplyStationService {
    //获取地点
    Result getPickupLocations(String supplyStationID) ;
    //获取销售记录
    Result getSalesStatistics(String stationId) ;
    // 查看补货申请 TODO
    Result viewRestockRequests();
    // 处理补货申请 TODO
    Result processRestockRequest(String requestId, List<DriverProduct> driverProducts);
    // 向司机发送补货邀约 TODO
    Result sendRestockInvitation(String driverId, List<DriverProduct> suggestedDriverProducts);
    // 供货站人员拍照提交商品 TODO
    Result uploadProductImage(String productId, Image productImage);
    //删除商品
    Result deleteSupplyStationProduct(String id);
    //获取商品
    Result getSupplyStationProductDetails(String productId , String stationId);
    //添加商品
    Result addSupplyStationProduct(String productId ,double productPrice ,Integer stockQuantity) ;
    //批量修改商品库存
    Result updateSupplyStationProductBatch(List<DriverProduct> driverProductList);
    //获取当天的收入记录
    Result getTodayRevenue() ;
    //获取一周的收入记录
    Result getWeeklyRevenue() ;
    //获取一月的收入记录
    Result getMonthlyRevenue() ;
    //获取一年的收入记录
    Result getYearlyRevenue() ;
    //获取总的收入记录
    Result getTotalRevenue() ;
    //TODO 是否还要添加一个查找商品的接口？
    // 查看供货站库存的类型信息
    // 查看供货站的员工列表
    Result viewStaffList(String stationId);
    // 查看供货站信息
    Result getStationProfile(String stationId);
    // 更新供货站信息
    Result updateStationProfile(SupplyStation supplyStation);
    // 查看库存信息
    Result viewInventory(String stationId);
    // 通过类型查看供货站的库存信息
    Result viewSupplyStationInventoryByCategory(String stationId, String category);
    Result insertStaff(StationStaff stationStaff) ;
    Result updateStaff(StationStaff stationStaff) ;
    Result viewSupplyStationCategories(String stationId) ;
    Result updateInventory(String stationId, List<SupplyStationStore> supplyStationStoreList) ;
    // 司机修改汽车牌照
    Result updateStationStaffRole(String role , String stationStaffId);
}
