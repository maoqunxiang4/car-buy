package com.xiaomaotongzhi.randomBuy.service;

import com.xiaomaotongzhi.randomBuy.entity.Addition.*;
import com.xiaomaotongzhi.randomBuy.utils.Result;

import java.util.List;

public interface IDriverService {
    //添加汽车牌照
    Result insertDevice(Device device) ;
    // 修改汽车牌照
    Result updateDriverVehicle(Device device);
    // 提交补货申请 TODO 这个使用消息队列kafaka或者是Netty （未完成）
    Result requestRestock(List<DriverProduct> driverProducts);
    // 查看广告播放统计
    Result getDriverAdPlaybackStatistics();
    // 查看当天营销数据
    Result getDriverDailySalesData();
    // 查找最近的供货站
    Result findNearestSupplyStations(double x ,double y);
    // 响应供货站的补货邀约（未完成） TODO
    Result respondToRestockInvitation(String invitationId, boolean accept);
    // 创建或更新优惠券
    Result insertCoupon(Coupon coupon);
    // 更新优惠券
    Result updateCoupon(Coupon coupon);
//    // 查看优惠券的使用统计
//    Result viewCouponUsageStatistics(String couponId);
//    // 查看与供货站合作的广告商列表
//    Result viewAdvertisers(String stationId);
//    // 查看供货站的员工列表
//    Result viewStaffList(String stationId);
//    // 添加供货站员工信息
//    Result insertStaff(StationStaff stationStaff);
//    // 更新供货站员工信息
//    Result updateStaff(StationStaff stationStaff);
//    // 查看供货站信息
//    Result getStationProfile(String stationId);
//    // 更新供货站信息
//    Result updateStationProfile(SupplyStation supplyStation);
//    // 查看库存信息
//    Result viewInventory(String stationId);
//    // 更新库存信息(未完成)
//    Result updateInventory(String stationId, List<SupplyStationStore> supplyStationStoreList);
//    // 通过类型查看供货站的库存信息
//    Result viewSupplyStationInventoryByCategory(String stationId, String category);
//    // 查看供货站库存的类型信息
//    Result viewSupplyStationCategories(String stationId);
    //按照类别列举司机拥有的商品
    Result ListDriverProductsByCategory(String category ,String driverId) ;
    //列举司机拥有的商品的所有类别
    Result ListDriverAdditionCategory(String driverId) ;
}
