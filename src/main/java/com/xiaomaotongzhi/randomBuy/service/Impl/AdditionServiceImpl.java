package com.xiaomaotongzhi.randomBuy.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiaomaotongzhi.randomBuy.dto.*;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Advertisement;
import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.entity.Addition.OrderDriver;
import com.xiaomaotongzhi.randomBuy.entity.DriverAdvertisement;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.Template.AdvertisementTemplate;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.Template.ProductTemplate;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.Template.TemplateFactory;
import com.xiaomaotongzhi.randomBuy.entity.Person.Driver;
import com.xiaomaotongzhi.randomBuy.entity.Person.Passenger;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyDriverAdPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.AdTotalPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.mapper.*;
import com.xiaomaotongzhi.randomBuy.service.IAdditionService;
import com.xiaomaotongzhi.randomBuy.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.xiaomaotongzhi.randomBuy.utils.MapKey.*;
import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.convertLocalDateTime;
import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNowStr;

@Service
public class AdditionServiceImpl implements IAdditionService {
    private MapperFactory mapperFactory = MapperFactory.getInstance() ;

    @Autowired
    private DriverProductMapper driverProductMapper;

    @Autowired
    private OrderDriverMapper orderMapper ;

    @Autowired
    private AdvertisementMapper advertisementMapper ;

    @Autowired
    private DailyViewsRecordMapper dailyViewsRecordMapper ;

    @Autowired
    private DailyDriverAdPlaybackRecordMapper dailyDriverAdPlaybackRecordMapper;

    @Autowired
    private AdTotalPlaybackRecordMapper adTotalPlaybackRecordMapper;

    @Autowired
    private TemplateFactory templateFactory ;

    @Autowired
    private PassengerMapper passengerMapper ;

    @Autowired
    private DriverMapper driverMapper ;

    @Autowired
    private DriverAdvertisementMapper driverAdvertisementMapper ;

    private final SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator() ;

    @Override
    public Result addProduct(DriverProduct driverProductInfo) {
        driverProductInfo.setId(String.valueOf(snowflakeIdGenerator.nextId()));
        int result = driverProductMapper.insert(driverProductInfo);
        if (result > 0) {
            return Result.success(200, "Product added successfully!");
        } else {
            return Result.fail(500, "Failed to add product.");
        }
    }

    @Override
    public Result updateProduct(DriverProduct driverProductInfo) {
        int result = driverProductMapper.update(driverProductInfo, new UpdateWrapper<DriverProduct>().eq("id", driverProductInfo.getId()));
        if (result > 0) {
            return Result.success(200, "Product updated successfully!");
        } else {
            return Result.fail(500, "Failed to update product.");
        }
    }

    @Override
    public Result deleteProduct(String productId) {
        int result = driverProductMapper.deleteById(productId);
        if (result > 0) {
            return Result.success(200, "Product deleted successfully!");
        } else {
            return Result.fail(500, "Failed to delete product.");
        }
    }

    @Override
    public Result listProducts(String driverId, int page, int size) {
        Page<DriverProduct> productPage = new Page<>(page, size);
        IPage<DriverProduct> resultPage = driverProductMapper.selectPage(productPage, new QueryWrapper<DriverProduct>().eq("driver_id", driverId));
        if (resultPage != null && resultPage.getRecords().size() > 0) {
            return Result.success(200, resultPage);
        } else {
            return Result.fail(404, "No products found for the given driverId.");
        }
    }

    @Override
    public Result getProductDetails(String productId) {
        DriverProduct driverProduct = driverProductMapper.selectById(productId);
        if (driverProduct != null) {
            return Result.success(200, driverProduct);
        } else {
            return Result.fail(404, "Product not found.");
        }
    }

    @Override
    public Result buyProduct(DriverProduct driverProduct, Double price , Integer amount , Driver driver , String x , String y) {
        SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator();
        HashMap<String, String> map = new HashMap<>();
        map.put(PRODUCT_ID , driverProduct.getId());
        map.put(PASSENGER_ID , UserHolder.getPerson().getOpenid()) ;
        map.put(DRIVER_ID , driverProduct.getDriver_id()) ;
        map.put(ORDER_PRICE , String.valueOf(price)) ;
        map.put(ORDER_DATE , getTimeNowStr()) ;
        map.put(AMOUNT , String.valueOf(amount)) ;
        map.put(VEHICLE_ID , driver.getVehicleId()) ;
        map.put(SALE_PRICE , String.valueOf(driverProduct.getPrice())) ;
        map.put(SALES_RECORD_ID , String.valueOf(snowflakeIdGenerator.nextId())) ;
        map.put(USER_BEHAVIOR_RECORD_ID , String.valueOf(snowflakeIdGenerator.nextId())) ;
        map.put(X , x) ;
        map.put(Y , y) ;

        //生成订单,添加记录
        ProductTemplate productTemplate =(ProductTemplate) templateFactory.getTemplateBean("productTemplate");
        productTemplate.additionDoingTemplate(map);
        return Result.success(200);
    }

    @Override
    public Result listUserOrders(String passengerId) {
        List<OrderDriver> driverOrders = orderMapper
                .selectList(new QueryWrapper<OrderDriver>().eq("passenger_id", passengerId));
        ArrayList<UserOrderDTO> userOrderDTOS = new ArrayList<>();
        for (OrderDriver driverOrder : driverOrders) {
            Driver driver = driverMapper.selectById(driverOrder.getDriverId());
            userOrderDTOS.add(UserOrderDTO.builder()
                    .productName(driver.getUsername()).orderDate(convertLocalDateTime(driverOrder.getOrderDate()))
                    .build()) ;
        }
        return Result.success(200, userOrderDTOS);
    }

    @Override
    public Result getOrderDetails(String orderId) {
        OrderDriver driverOrder = orderMapper.selectById(orderId);
        Passenger passenger = passengerMapper.selectById(driverOrder.getPassengerId());
        DriverProduct driverProduct = driverProductMapper.selectById(driverOrder.getProductId());
        Driver driver = driverMapper.selectById(driverOrder.getDriverId());
        DriverOrderDTO driverOrderDTO = DriverOrderDTO.builder().productName(driverProduct.getProductName())
                .passengerName(passenger.getUsername()).driverName(driver.getUsername())
                .orderDate(convertLocalDateTime(driverOrder.getOrderDate())).amount(driverOrder.getAmount())
                .vehicleId(driverOrder.getVehicleId()).build();
        return Result.success(200, driverOrderDTO);
    }

    @Override
    public Result listAdsForDriver(String driverId) {
        List<DriverAdvertisement> driverAdvertisements =
                driverAdvertisementMapper.selectList(new QueryWrapper<DriverAdvertisement>().eq("driver_id", driverId));
        List<String> advertisementIDList = driverAdvertisements
                .stream().map(DriverAdvertisement::getAdvertisementId).collect(Collectors.toList());
        List<Advertisement> advertisements =
                advertisementMapper.selectBatchIds(advertisementIDList) ;
        List<AdvertisementDTO> advertisementDTOS = advertisements.stream().map(advertisement -> {
            AdvertisementDTO advertisementDTO = new AdvertisementDTO();
            advertisementDTO.setTitle(advertisement.getTitle());
            advertisementDTO.setContent(advertisement.getContent());
            advertisementDTO.setVertiser(advertisement.getVertiser());
            return advertisementDTO;
        }).collect(Collectors.toList());
        return Result.success(200 ,advertisementDTOS);
    }

    @Override
    public Result watchAdvertisement(String adId, String driverId) {
        HashMap<String, String> map = new HashMap<>();
        Advertisement advertisement = advertisementMapper.selectById(adId);
        map.put(ADVERTISEMENT_ID , adId) ;
        map.put(ADVERTISEMENT_TITLE , advertisement.getTitle()) ;
        map.put(ADVERTISEMENT_CONTENT , advertisement.getContent()) ;
        map.put(ADVERTISEMENT_VERTISER , advertisement.getVertiser()) ;
        map.put(ADVERTISEMENT_STARTDATE , String.valueOf(advertisement.getStartDate())) ;
        map.put(ADVERTISEMENT_ENDDATE , String.valueOf(advertisement.getEndDate())) ;
        map.put(ADVERTISEMENT_TARGETAUDIENCE , advertisement.getTargetAudience() ) ;
        map.put(ADVERTISEMENT_TOTAL_ID , adId) ;
        map.put(ADVERTISEMENT_DRIVER_DAILY_ID , adId) ;
        map.put(DRIVER_ID, driverId);
        //产生购买行为
        AdvertisementTemplate advertisementTemplate = (AdvertisementTemplate) templateFactory.getTemplateBean("advertisementTemplate");
        advertisementTemplate.additionDoingTemplateOnlyOne(map);
        return Result.success(200);
    }

//    @Override
//    public Result recordAdPlayback(String adId, String driverId) {
//        HashMap<String, String> map = new HashMap<>();
//        map.put(ADVERTISEMENT_TOTAL_ID , adId) ;
//        map.put(ADVERTISEMENT_DRIVER_DAILY_ID , adId) ;
//        map.put(DRIVER_ID, driverId);
//        //产生购买行为
//        AdvertisementTemplate advertisementTemplate = (AdvertisementTemplate) templateFactory.getTemplateBean("advertisementTemplate");
//        advertisementTemplate.additionDoingTemplateOnlyOne(map);
//        return Result.success(200);
//    }

    @Override
    public Result addAd(Advertisement adInfo) {
        adInfo.setId(String.valueOf(snowflakeIdGenerator.nextId()));
        int result = advertisementMapper.insert(adInfo);
        if (result > 0) {
            return Result.success(200 ,"Advertisement added successfully!");
        } else {
            return Result.fail(500 ,"Failed to add advertisement.");
        }
    }

    @Override
    public Result updateAd(Advertisement adInfo) {
        int result = advertisementMapper.updateById(adInfo);
        if (result > 0) {
            return Result.success(200 ,"Advertisement updated successfully!");
        } else {
            return Result.fail(500 ,"Failed to update advertisement.");
        }
    }

    @Override
    public Result deleteAd(String adId) {
        int result = advertisementMapper.deleteById(adId);
        if (result > 0) {
            return Result.success(200, "Advertisement deleted successfully!");
        } else {
            return Result.fail(500, "Failed to delete advertisement.");
        }
    }

//    @Override
//    public Result listAdPlaybacks(String driverId) {
//        //TODO
//        // Assuming you have a method in AdvertisementMapper to fetch playbacks for an ad
//        List<DriverAdvertisement> driverAdvertisements =
//                driverAdvertisementMapper.selectList(new QueryWrapper<DriverAdvertisement>().eq("driver_id", driverId));
//        List<String> advertisementIDList = driverAdvertisements
//                .stream().map(DriverAdvertisement::getAdvertisementId).collect(Collectors.toList());
//        List<Advertisement> advertisements =
//                advertisementMapper.selectBatchIds(advertisementIDList) ;
//        List<AdvertisementDTO> advertisementDTOS = advertisements.stream().map(advertisement -> {
//            AdvertisementDTO advertisementDTO = new AdvertisementDTO();
//            advertisementDTO.setTitle(advertisement.getTitle());
//            advertisementDTO.setContent(advertisement.getContent());
//            advertisementDTO.setVertiser(advertisement.getVertiser());
//            return advertisementDTO;
//        }).collect(Collectors.toList());
//        if (advertisementDTOS != null && ! advertisements.isEmpty()) {
//            return Result.success(200, advertisementDTOS);
//        } else {
//            return Result.fail(404, "No playbacks found for the given adId.");
//        }
//    }

    @Override
    public Result getAdPlaybackStatistics(String driverId) {
        // 获取当日的播放记录
        List<DailyDriverAdPlaybackRecord> dailyPlaybacks = dailyDriverAdPlaybackRecordMapper.selectList(
                new QueryWrapper<DailyDriverAdPlaybackRecord>().eq("driver_id", driverId)
        );
        List<String> adIdList = dailyPlaybacks.stream().map(DailyDriverAdPlaybackRecord::getAdId).collect(Collectors.toList());
        List<Advertisement> advertisementList = advertisementMapper.selectBatchIds(adIdList);
        ArrayList<DailyDriverAdPlaybackRecordDTO> dailyDriverAdPlaybackRecordDTOS = new ArrayList<>();
        HashMap<String, DailyDriverAdPlaybackRecord> dailyDriverAdPlaybackRecordHashMap = new HashMap<>();
        for (Advertisement advertisement : advertisementList) {
            for (DailyDriverAdPlaybackRecord dailyPlayback : dailyPlaybacks) {
                if (advertisement.getId().equals(dailyPlayback.getAdId())) {
                    dailyDriverAdPlaybackRecordHashMap.put(advertisement.getId(), dailyPlayback) ;
                }
            }
        }

        advertisementList.forEach(advertisement -> {
            DailyDriverAdPlaybackRecord dailyDriverAdPlaybackRecord = dailyDriverAdPlaybackRecordHashMap.get(advertisement.getId());
            DailyDriverAdPlaybackRecordDTO dailyDriverAdPlaybackRecordDTO = DailyDriverAdPlaybackRecordDTO.builder()
                    .title(advertisement.getTitle())
                    .content(advertisement.getContent())
                    .views((long) dailyDriverAdPlaybackRecord.getNumberOfViews())
                    .id(advertisement.getId())
                    .build();
            dailyDriverAdPlaybackRecordDTOS.add(dailyDriverAdPlaybackRecordDTO);
        });

        // 获取整体的播放记录
        List<AdTotalPlaybackRecord> adTotalPlaybackRecordList = adTotalPlaybackRecordMapper.selectList(
                new QueryWrapper<AdTotalPlaybackRecord>().in("addition_id", adIdList)
        );
        ArrayList<AdTotalPlaybackRecordDTO> adTotalPlaybackRecordDTOS = new ArrayList<>();
        HashMap<String, AdTotalPlaybackRecord> adTotalPlaybackRecordHashMap = new HashMap<>();
        for (Advertisement advertisement : advertisementList) {
            for (AdTotalPlaybackRecord adTotalPlaybackRecord : adTotalPlaybackRecordList) {
                if (advertisement.getId().equals(adTotalPlaybackRecord.getAdditionId())) {
                    adTotalPlaybackRecordHashMap.put(advertisement.getId(), adTotalPlaybackRecord) ;
                }
            }
        }

        advertisementList.forEach(advertisement -> {
            AdTotalPlaybackRecord adTotalPlaybackRecord = adTotalPlaybackRecordHashMap.get(advertisement.getId());
            AdTotalPlaybackRecordDTO adTotalPlaybackRecordDTO = AdTotalPlaybackRecordDTO.builder().title(advertisement.getTitle())
                    .content(advertisement.getContent()).views((long) adTotalPlaybackRecord.getTotalNumber())
                    .id(advertisement.getId()).build();
            adTotalPlaybackRecordDTOS.add(adTotalPlaybackRecordDTO) ;
        });

        // 使用DTO进行封装
        ArrayList<AdPlaybackStatisticsDTO> adPlaybackStatisticsDTOS = new ArrayList<>();
        for (DailyDriverAdPlaybackRecordDTO dailyDriverAdPlaybackRecordDTO : dailyDriverAdPlaybackRecordDTOS) {
            for (AdTotalPlaybackRecordDTO adTotalPlaybackRecordDTO : adTotalPlaybackRecordDTOS) {
                if (adTotalPlaybackRecordDTO.getId().equals(dailyDriverAdPlaybackRecordDTO.getId())) {
                    adPlaybackStatisticsDTOS.add(new AdPlaybackStatisticsDTO(dailyDriverAdPlaybackRecordDTO ,adTotalPlaybackRecordDTO )) ;
                }
            }
        }

        return Result.success(200, adPlaybackStatisticsDTOS);
    }
}
