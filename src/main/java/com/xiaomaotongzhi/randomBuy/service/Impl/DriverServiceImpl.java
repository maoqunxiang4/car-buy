package com.xiaomaotongzhi.randomBuy.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaotongzhi.randomBuy.dto.DailyDriverAdPlaybackRecordDTO;
import com.xiaomaotongzhi.randomBuy.dto.DailySalesDTO;
import com.xiaomaotongzhi.randomBuy.entity.Addition.*;
import com.xiaomaotongzhi.randomBuy.entity.Person.Driver;
import com.xiaomaotongzhi.randomBuy.entity.Person.PersonGathering;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyDriverAdPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.mapper.*;
import com.xiaomaotongzhi.randomBuy.service.IDriverService;
import com.xiaomaotongzhi.randomBuy.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNow;

@Service
public class DriverServiceImpl implements IDriverService {
    @Autowired
    private DriverProductMapper driverProductMapper;

    @Autowired
    private SupplyStationMapper supplyStationMapper ;

    private MapperFactory mapperFactory = MapperFactory.getInstance();

    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Autowired
    private PassengerMapper passengerMapper ;

    @Autowired
    private DriverMapper driverMapper ;

    @Autowired
    private StationStaffMapper stationStaffMapper;

    @Autowired
    private OrderDriverMapper orderMapper ;

    @Autowired
    private DailyDrivrAdPlaybackRecordMapper dailyDrivrAdPlaybackRecordMapper ;

    @Autowired
    private CouponMapper couponMapper ;

    @Autowired
    private DeviceMapper deviceMapper ;

    @Autowired
    private UserCouponMapper userCouponMapper ;

    @Autowired
    private SupplyStationStoreMapper supplyStationStoreMapper ;

    @Autowired
    private DailySalesRecordMapper dailySalesRecordMapper ;

    @Autowired
    private AdvertisementMapper advertisementMapper ;

    @Autowired
    private OrderDriverMapper orderDriverMapper ;

    @Autowired
    private ProductMapper productMapper;

    private SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator() ;

    PersonGathering personGathering = PersonGathering.getInstance() ;

    @Override
    public Result insertDevice(Device device) {
        device.setId(String.valueOf(snowflakeIdGenerator.nextId()));
        device.setDriverId(UserHolder.getPerson().getOpenid());
        device.setLastCheck(getTimeNow());
        int result = deviceMapper.insert(device);
        if (result==0) {
            return Result.fail(400,"插入失败") ;
        }
        return Result.success(200);
    }

    @Override
    public Result updateDriverVehicle(Device device) {
        device.setDriverId(UserHolder.getPerson().getOpenid());
        device.setLastCheck(getTimeNow());
        int result = deviceMapper
                .update(device , new QueryWrapper<Device>().eq("driver_id", device.getDriverId())) ;
        if (result > 0) {
            return Result.success(200);
        } else {
            return Result.fail(400,"Failed to update device.");
        }
    }

    @Override
    public Result requestRestock(List<DriverProduct> driverProducts) {
        //TODO 使用kafaka或者是Netty
        return Result.success(200);
    }

    @Override
    public Result getDriverAdPlaybackStatistics() {
        List<DailyDriverAdPlaybackRecord> dailyDriverAdPlaybackRecords = dailyDrivrAdPlaybackRecordMapper
                .selectList(new QueryWrapper<DailyDriverAdPlaybackRecord>().eq("driver_id", UserHolder.getPerson().getOpenid()));
        List<String> adIdList = dailyDriverAdPlaybackRecords.stream().map(DailyDriverAdPlaybackRecord::getAdId).collect(Collectors.toList());
        List<Advertisement> advertisements = advertisementMapper.selectBatchIds(adIdList);
        ArrayList<DailyDriverAdPlaybackRecordDTO> dailyDriverAdPlaybackRecordDTOS = new ArrayList<>();
        for (DailyDriverAdPlaybackRecord dailyDriverAdPlaybackRecord : dailyDriverAdPlaybackRecords) {
            for (Advertisement advertisement : advertisements) {
                if (advertisement.getId().equals(dailyDriverAdPlaybackRecord.getAdId())) {
                    dailyDriverAdPlaybackRecordDTOS.add(DailyDriverAdPlaybackRecordDTO.builder().id(dailyDriverAdPlaybackRecord.getId())
                            .content(advertisement.getContent()).title(advertisement.getTitle())
                            .views((long) dailyDriverAdPlaybackRecord.getNumberOfViews()).build()) ;
                }
            }
        }
        return Result.success(200 , dailyDriverAdPlaybackRecordDTOS) ;
    }

    @Override
    public Result getDriverDailySalesData() {
        // 假设有一个DailySalesDataMapper
        // 不需要创建新的表用于记录当天的售卖情况，因为order表中就可以查询到
        ArrayList<DailySalesDTO> dailySalesDTOS = new ArrayList<>();
//TODO 出问题了
        List<OrderDriver> orderDrivers
                = orderDriverMapper.selectList(new QueryWrapper<OrderDriver>()
                        .eq("order_date", getTimeNow().toLocalDate())
                        .eq("driver_id", UserHolder.getPerson().getOpenid()));
        for (OrderDriver orderDriver : orderDrivers) {
            DriverProduct driverProduct = driverProductMapper.selectById(orderDriver.getProductId());
            Driver driver = driverMapper.selectById(orderDriver.getDriverId());
            dailySalesDTOS.add(DailySalesDTO.builder().productName(driverProduct.getProductName())
                    .driverName(driver.getUsername()).orderDate(orderDriver.getOrderDate())
                    .amount(orderDriver.getAmount()).price(orderDriver.getPrice())
                    .build()) ;
        }
        return Result.success(200,dailySalesDTOS);
    }

    @Override
    public Result findNearestSupplyStations(double x, double y) {
        //TODO  使用地理位置查询最近的供货站，这里简化处理
//        List<SupplyStation> stations = supplyStationMapper.selectNearestStations(x, y);
        return Result.success(200);
    }

    @Override
    public Result respondToRestockInvitation(String invitationId, boolean accept) {
        //TODO 假设有一个RestockInvitationMapper，使用kafaka和Netty
        return Result.success(200);
    }

    @Override
    public Result insertCoupon(Coupon coupon) {
        coupon.setId(String.valueOf(snowflakeIdGenerator.nextId()));
        coupon.setStatus(StatusEnum.ACTIVE) ;
        coupon.setCreatedAt(getTimeNow());
        coupon.setUpdatedAt(getTimeNow());
        int result = couponMapper.insert(coupon);
        if (result==0) {
            return Result.success(400,"插入失败");
        }
        return Result.success(200);
    }

    @Override
    public Result updateCoupon(Coupon coupon) {
        coupon.setUpdatedAt(getTimeNow());
        if (coupon.getStatus().equals(StatusEnum.ACTIVE) ) {
            coupon.setStatus(StatusEnum.ACTIVE);
        } else if (coupon.getStatus().equals(StatusEnum.UN_ACTIVITY)) {
            coupon.setStatus(StatusEnum.UN_ACTIVITY);
        }
        int result = couponMapper.updateById(coupon);
        if (result==0) {
            return Result.fail(400 , "更新失败") ;
        }
        return Result.success(200);
    }

//    @Override
//    public Result viewCouponUsageStatistics(String couponId) {
//        // 假设有一个CouponUsageStatisticsMapper
//        CouponUsageStatistics stats = couponUsageStatisticsMapper.selectByCouponId(couponId);
//        return Result.success(stats);
//    }

    @Override
    public Result ListDriverProductsByCategory(String category ,String driverId) {
        List<DriverProduct> driverProductList = driverProductMapper.selectList(new QueryWrapper<DriverProduct>().eq("driver_id", driverId));
        List<String> categoryList = UsualUtils.getCategoryListByProducts(driverProductList);
        return Result.success(200,categoryList);
    }

    @Override
    public Result ListDriverAdditionCategory(String driverId) {
        List<DriverProduct> driverProductList = driverProductMapper.selectList(new QueryWrapper<DriverProduct>().eq("driver_id", driverId));
        return Result.success(200 , UsualUtils.getCategoryListByProducts(driverProductList)) ;
    }
}
