package com.xiaomaotongzhi.randomBuy.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiaomaotongzhi.randomBuy.dto.*;
import com.xiaomaotongzhi.randomBuy.entity.Addition.*;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Builder.AdditionDirecter;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.Template.TemplateFactory;
import com.xiaomaotongzhi.randomBuy.entity.Person.PersonGathering;
import com.xiaomaotongzhi.randomBuy.entity.Person.StationStaff;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.SupplyStationDailySalesRecord;
import com.xiaomaotongzhi.randomBuy.mapper.*;
import com.xiaomaotongzhi.randomBuy.service.ISupplyStationService;
import com.xiaomaotongzhi.randomBuy.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNow;

@Service
public class SupplyStationImplService implements ISupplyStationService {
    private MapperFactory mapperFactory = MapperFactory.getInstance();

    private TemplateFactory templateFactory = new TemplateFactory();

    @Autowired
    private SupplyStationMapper supplyStationMapper ;

    @Autowired
    private DriverProductMapper driverProductMapper;

    @Autowired
    private SupplyStationDailySalesRecordMapper supplyStationDailySalesRecordMapper ;

    @Autowired
    private SupplyStationStoreMapper supplyStationStoreMapper ;

    @Autowired
    private StationStaffMapper stationStaffMapper ;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private SupplyStationOrderMapper supplyStationOrderMapper ;

    private final SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator() ;

    private final AdditionDirecter additionDirecter = AdditionDirecter.getInstance() ;

    private final PersonGathering personGathering = PersonGathering.getInstance() ;

    @Override
    public Result getPickupLocations(String supplyStationID) {
        SupplyStation supplyStation = supplyStationMapper
                .selectOne(new QueryWrapper<SupplyStation>().eq("id", supplyStationID));
        SupplyStationLocationDTO supplyStationLocationDTO = SupplyStationLocationDTO.builder().location(supplyStation.getLocation())
                .x(String.valueOf(supplyStation.getX())).y(String.valueOf(supplyStation.getY())).build();
        return Result.success(200,supplyStationLocationDTO) ;
    }

    @Override
    public Result getSalesStatistics(String stationId) {
        SalesStatisticsDTO salesStatisticsDTO = new SalesStatisticsDTO();
        salesStatisticsDTO.setStationId(stationId) ;
        return Result.success(200, salesStatisticsDTO);
    }

    @Override
    public Result viewRestockRequests() {
        return null;
    }

    @Override
    public Result processRestockRequest(String requestId, List<DriverProduct> driverProducts) {
        return null;
    }

    @Override
    public Result sendRestockInvitation(String driverId, List<DriverProduct> suggestedDriverProducts) {
        return null;
    }

    @Override
    public Result uploadProductImage(String productId, Image productImage) {
        return null;
    }

    @Override
    public Result deleteSupplyStationProduct(String id) {
        int result = productMapper.deleteById(id);
        if (result==0) {
            return Result.fail(400,"删除失败") ;
        }
        return Result.success(200);
    }

    @Override
    public Result getSupplyStationProductDetails(String productId , String stationId) {
        //查找supplyStationProduct
        Product product = productMapper
                .selectOne(new QueryWrapper<Product>()
                        .eq("id",productId)) ;
        //查找supplyStation
        SupplyStation supplyStation = supplyStationMapper
                .selectOne(new QueryWrapper<SupplyStation>().eq("id", stationId));
        //查找supplyStationStore
        SupplyStationStore supplyStationStore = supplyStationStoreMapper
                .selectOne(new QueryWrapper<SupplyStationStore>()
                        .eq("supply_station_id", stationId).eq("product_id", productId));
        //建立SupplyStationInfoDTO
        SupplyStationProductInfoDTO supplyStationProductInfoDTO = SupplyStationProductInfoDTO.builder().productName(product.getProductName())
                .description(product.getDescription()).price(product.getPrice())
                .imageUrl(product.getImageUrl()).category(product.getCategory())
                .location(supplyStation.getLocation()).stockQuantity(supplyStationStore.getStockQuantity())
                .lastUpdated(supplyStationStore.getLastUpdated()).build();
        return Result.success(200, supplyStationProductInfoDTO);
    }

    @Override
    public Result addSupplyStationProduct(String productId ,double productPrice ,Integer stockQuantity) {
        try {
            //查找商品
            Product findProduct = productMapper.selectById(productId);

            //获取当前员工数据
            StationStaff stationStaff = stationStaffMapper.selectById(UserHolder.getPerson().getOpenid());
            if (stationStaff == null) {
                return Result.fail(404, "Station staff not found");
            }
            String stationId = stationStaff.getStationId();

            if (findProduct !=null) {
                //更新库存
                SupplyStationStore supplyStationStore = new SupplyStationStore();
                supplyStationStoreMapper.update(supplyStationStore, new UpdateWrapper<SupplyStationStore>()
                        .eq("product_id", findProduct.getId())
                        .setSql("stock_quantity = stock_quantity + " + stockQuantity));
            } else {
                SupplyStationStore supplyStationStore = additionDirecter.buildSupplyStationStore(String.valueOf(snowflakeIdGenerator.nextId()), stationId
                        , productId, Long.valueOf(stockQuantity), getTimeNow());
                supplyStationStoreMapper.insert(supplyStationStore) ;
            }
            return Result.success(200);
        } catch (Exception e) {
            // Log the exception for debugging purposes
            e.printStackTrace();
            return Result.fail(500, "Internal server error");
        }
    }

    @Override
    public Result updateSupplyStationProductBatch(List<DriverProduct> driverProductList) {
        try {
            // 获取当前员工数据
            StationStaff stationStaff = stationStaffMapper.selectById(UserHolder.getPerson().getOpenid());
            if (stationStaff == null) {
                return Result.fail(404, "Station staff not found");
            }
            String stationId = stationStaff.getStationId();

            for (DriverProduct driverProduct : driverProductList) {
                // 查找商品
                DriverProduct existingDriverProduct = driverProductMapper.selectById(driverProduct.getId());
                if (existingDriverProduct == null) {
                    return Result.fail(404, "Product not found with ID: " + driverProduct.getId());
                }

                // 更新库存
                SupplyStationStore supplyStationStore = supplyStationStoreMapper.selectOne(
                        new QueryWrapper<SupplyStationStore>().eq("product_id", driverProduct.getId()).eq("supply_station_id", stationId)
                );
                if (supplyStationStore != null) {
                    supplyStationStore.setStockQuantity(supplyStationStore.getStockQuantity() + driverProduct.getStock());
                    supplyStationStoreMapper.updateById(supplyStationStore);
                }

                // 更新供应站产品数据
                Product product = productMapper.selectOne(
                        new QueryWrapper<Product>().eq("id", driverProduct.getId())
                );
                if (product != null) {
                    product.setProductName(driverProduct.getProductName());
                    product.setDescription(driverProduct.getDescription());
                    product.setPrice(driverProduct.getPrice());
                    product.setImageUrl(driverProduct.getImageUrl());
                    product.setCategory(driverProduct.getCategory());
                    product.setUpdatedAt(getTimeNow()); // Assuming you have a method getTimeNow() to get current time
                    productMapper.updateById(product);
                }
            }

            return Result.success(200, "Products updated successfully");
        } catch (Exception e) {
            // Log the exception for debugging purposes
            e.printStackTrace();
            return Result.fail(500, "Internal server error");
        }
    }

    @Override
    public Result getTodayRevenue() {
        String openid = UserHolder.getPerson().getOpenid();
        StationStaff stationStaff = stationStaffMapper.selectById(openid);
        List<SupplyStationDailySalesRecord> supplyStationDailySalesRecords = supplyStationDailySalesRecordMapper
                .selectList(new QueryWrapper<SupplyStationDailySalesRecord>()
                        .eq("date", getTimeNow().toLocalDate()).eq("station_id" , stationStaff.getStationId()));
        double sum = supplyStationDailySalesRecords.stream().mapToDouble(SupplyStationDailySalesRecord::getDailyRevenue).sum();
        return Result.success(200,sum) ;
    }

    @Override
    public Result getWeeklyRevenue() {
        LocalDate monday = DateUtils.getLastWeekMonday();
        LocalDate sunday = DateUtils.getLastWeekSunday();
        QueryWrapper<SupplyStationDailySalesRecord> query = new QueryWrapper<>();
        query.between("date", monday, sunday);
        return Result.success(200,supplyStationDailySalesRecordMapper
                .selectList(query).stream().mapToDouble(SupplyStationDailySalesRecord::getDailyRevenue).sum());
    }

    @Override
    public Result getMonthlyRevenue() {
        LocalDate today = LocalDate.now();
        LocalDate monthStart = today.minusMonths(1).withDayOfMonth(1);
        LocalDate monthEnd = today.minusMonths(1).withDayOfMonth(today.minusMonths(1).lengthOfMonth());
        QueryWrapper<SupplyStationDailySalesRecord> query = new QueryWrapper<>();
        query.between("date", monthStart, monthEnd);
        return Result.success(200,supplyStationDailySalesRecordMapper.selectList(query)
                .stream().mapToDouble(SupplyStationDailySalesRecord::getDailyRevenue).sum());
    }

    @Override
    public Result getYearlyRevenue() {
        LocalDate today = LocalDate.now();
        LocalDate yearStart = today.minusYears(1).withDayOfYear(1);
        LocalDate yearEnd = today.minusYears(1).withDayOfYear(today.minusYears(1).lengthOfYear());
        QueryWrapper<SupplyStationDailySalesRecord> query = new QueryWrapper<>();
        query.between("date", yearStart, yearEnd);
        return Result.success(200,supplyStationDailySalesRecordMapper.selectList(query).stream().mapToDouble(SupplyStationDailySalesRecord::getDailyRevenue).sum());
    }

    @Override
    public Result getTotalRevenue() {
        StationStaff stationStaff = stationStaffMapper.selectById(UserHolder.getPerson().getOpenid());
        List<SupplyStationOrder> supplyStationOrderList = supplyStationOrderMapper
                .selectList(new QueryWrapper<SupplyStationOrder>().eq("supply_station_id", stationStaff.getStationId()));
        return Result.success(200,supplyStationOrderList.stream().mapToDouble(SupplyStationOrder::getAmount).sum());
    }


    @Override
    public Result viewStaffList(String stationId) {
        List<StationStaff> staffList
                = stationStaffMapper.selectList
                (new QueryWrapper<StationStaff>().eq("station_id" , stationId)) ;
        SupplyStation supplyStation = supplyStationMapper.selectById(stationId);
        List<StationStaffDTO> stationStaffDTOList = staffList.stream().map(stationStaff -> {
            return StationStaffDTO.builder().username(stationStaff.getUsername()).mobile(stationStaff.getMobile())
                    .email(stationStaff.getEmail()).address(stationStaff.getAddress())
                    .stationName(supplyStation.getStationName())
                    .role(stationStaff.getRole()).build() ;
        }).collect(Collectors.toList());
        return Result.success(200,stationStaffDTOList);
    }

    //TODO
    @Override
    public Result insertStaff(StationStaff stationStaff) {
        stationStaff.setRegister_date(getTimeNow());
        stationStaff.setLast_login(getTimeNow());
        stationStaffMapper.insert(stationStaff);
        return Result.success(200) ;
    }

    @Override
    public Result updateStaff(StationStaff stationStaff) {
        stationStaff.setLast_login(getTimeNow());
        stationStaffMapper.updateById(stationStaff);
        return Result.success(200) ;
    }

    @Override
    public Result getStationProfile(String stationId) {
        SupplyStation supplyStation = supplyStationMapper
                .selectOne(new QueryWrapper<SupplyStation>().eq("id", stationId));
        return Result.success(200,supplyStation);
    }

    @Override
    public Result updateStationProfile(SupplyStation supplyStation) {
        int result = supplyStationMapper.updateById(supplyStation);
        if (result == 1) {
            return Result.success(200);
        } else {
            return Result.success(400,"更新失败");
        }
    }

    @Override
    public Result viewInventory(String stationId) {
        List<SupplyStationStore> supplyStationStoreList =
                supplyStationStoreMapper.selectList(new QueryWrapper<SupplyStationStore>().eq("supply_station_id",stationId));
        List<String> productIdList =
                supplyStationStoreList.stream().map(SupplyStationStore::getProductId).collect(Collectors.toList());
        List<Product> products = productMapper.selectBatchIds(productIdList);
        List<DriverProduct> driverProducts = driverProductMapper.selectBatchIds(productIdList);
        ArrayList<SupplyStationStoreDTO> supplyStationStoreDTOS = new ArrayList<>();
        for (Product product : products) {
            SupplyStationStore supplyStationStore = supplyStationStoreList.stream().filter(item -> item.getProductId().equals(product.getId())).findFirst().orElse(null);
            if (supplyStationStore != null) {
                supplyStationStoreDTOS.add(SupplyStationStoreDTO.builder().productName(product.getProductName())
                        .description(product.getDescription())
                        .price(product.getPrice()).stock(supplyStationStore.getStockQuantity())
                        .imageUrl(product.getImageUrl())
                        .category(product.getCategory()).build());
            }
        }
        return Result.success(200,supplyStationStoreDTOS);
    }



    @Override
    public Result viewSupplyStationInventoryByCategory(String stationId , String category) {
        List<SupplyStationStore> supplyStationStoreList = supplyStationStoreMapper
                .selectList(new QueryWrapper<SupplyStationStore>()
                        .eq("supply_station_id", stationId));
        List<String> productIdList = supplyStationStoreList.stream()
                .map(SupplyStationStore::getProductId).collect(Collectors.toList());
        List<Product> products = productMapper.selectList(new QueryWrapper<Product>()
                .eq("category", category).in("id", productIdList));
        ArrayList<SupplyStationProductDTO> supplyStationProductDTOS = new ArrayList<>();
        products.forEach(supplyStationProduct -> {
            supplyStationProductDTOS.add(
                    SupplyStationProductDTO.builder().
                            productName(supplyStationProduct.getProductName()).description(supplyStationProduct.getDescription()).
                            price(supplyStationProduct.getPrice()).imageUrl(supplyStationProduct.getImageUrl()).category(supplyStationProduct.getCategory()).build()
            );
        });
        return Result.success(200,supplyStationProductDTOS);
    }

    @Override
    public Result viewSupplyStationCategories(String stationId) {
        List<SupplyStationStore> supplyStationStoreList = supplyStationStoreMapper
                .selectList(new QueryWrapper<SupplyStationStore>()
                        .eq("supply_station_id", stationId));
        List<String> productIdList = supplyStationStoreList.stream()
                .map(SupplyStationStore::getProductId).collect(Collectors.toList());
        List<String> categoryList = UsualUtils.getCategoryListByIds(productIdList) ;
        return Result.success(200,categoryList);
    }

    @Override
    public Result updateInventory(String stationId, List<SupplyStationStore> supplyStationStoreList) {
        int i = supplyStationStoreMapper.updateSupplyStationStoreByList(supplyStationStoreList);
        if(i==0) {
            return Result.fail(400,"没有数据更新") ;
        }
        return Result.success(200);
    }

    @Override
    public Result updateStationStaffRole(String stationStaffId , String role) {
        String openid = UserHolder.getPerson().getOpenid();
        StationStaff stationStaff = (StationStaff) personGathering.getPerson(StationStaff.class);
        stationStaff.setRole(role);
        stationStaff.setId(openid);
        stationStaffMapper.updateById(stationStaff) ;
        return Result.success(200);
    }
}