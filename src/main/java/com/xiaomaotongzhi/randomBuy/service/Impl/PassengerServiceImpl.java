package com.xiaomaotongzhi.randomBuy.service.Impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiaomaotongzhi.randomBuy.dto.PassengerDTO;
import com.xiaomaotongzhi.randomBuy.entity.Adapter.*;
import com.xiaomaotongzhi.randomBuy.entity.Addition.*;
import com.xiaomaotongzhi.randomBuy.utils.MapperFactory;
import com.xiaomaotongzhi.randomBuy.entity.UserCoupon;
import com.xiaomaotongzhi.randomBuy.mapper.*;
import com.xiaomaotongzhi.randomBuy.utils.*;
import com.xiaomaotongzhi.randomBuy.entity.Person.*;
import com.xiaomaotongzhi.randomBuy.service.IPassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.xiaomaotongzhi.randomBuy.utils.Constants.*;
import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNow;

@Service
public class PassengerServiceImpl implements IPassengerService {
    @Value("${wx.app.id}")
    private String appId ;

    @Value("${wx.app.secret}")
    private String appSecret ;

    private MapperFactory mapperFactory = MapperFactory.getInstance() ;

    @Autowired
    private StringRedisTemplate stringRedisTemplate ;

    @Autowired
    private PassengerMapper passengerMapper ;

    @Autowired
    private DriverMapper driverMapper ;

    @Autowired
    private StationStaffMapper stationStaffMapper;

    @Autowired
    private OrderDriverMapper orderMapper ;

    @Autowired
    private CouponMapper couponMapper ;

    @Autowired
    private DeviceMapper deviceMapper ;

    @Autowired
    private UserCouponMapper userCouponMapper ;

    PersonGathering personGathering = PersonGathering.getInstance() ;

    @Override
    public Result login(String code) {
        String url = "https://api.weixin.qq.com/sns/jscode2session&appid=" + appId + "&secret=" + appSecret +
                "&js_code=" + code + "&grant_type=authorization_code" ;
        String UserJSONStr = HttpUtil.get(url);
        com.xiaomaotongzhi.randomBuy.utils.UserJSON userJSON =
                JSONUtil.toBean(UserJSONStr, com.xiaomaotongzhi.randomBuy.utils.UserJSON.class);
        if (userJSON.getOpenid() == null) {
            return Result.fail(400,"登陆失败") ;
        }
        PassengerMapper passengerMapper = (PassengerMapper) mapperFactory.getToolHandler("passengerMapper");
        Passenger passenger = passengerMapper.selectById(userJSON.getOpenid());
        if (Objects.isNull(passenger)){
            PassengerCreateUser passengerCreateUser = (PassengerCreateUser) AdapterFactory.getPersonAdapter(Passenger.class);
            passengerCreateUser.createUser(userJSON.getOpenid());
            passenger = passengerMapper.selectById(userJSON.getOpenid()) ;
        } else {
            //如果不为空，刷新登陆时间
            LocalDateTime now = getTimeNow() ;
            passenger.setLast_login(getTimeNow());
            passengerMapper.updateById(passenger) ;
            Driver driver = driverMapper.selectById(passenger.getId());
            StationStaff stationStaff = stationStaffMapper.selectById(passenger.getId());
            if (driver==null && stationStaff == null) {
                userJSON.setRole(PASSENGER);
            } else if (driver != null) {
                userJSON.setRole(DRIVER);
                driver.setLast_login(getTimeNow());
                driverMapper.updateById(driver) ;
            } else {
                userJSON.setRole(STATIONSTAFF);
                stationStaff.setLast_login(getTimeNow());
                stationStaffMapper.updateById(stationStaff) ;
            }
        }
        PassengerDTO passengerDTO = new PassengerDTO();
        BeanUtil.copyProperties(passenger , passengerDTO);
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(passengerDTO, new HashMap<>(),
                CopyOptions.create().setIgnoreNullValue(true).setFieldValueEditor((filedName, filedValue) -> {
                    if (filedValue != null) {
                        filedValue = filedValue.toString();
                    } else {
                        filedValue = "";
                    }
                    return filedValue;
                }));
        stringRedisTemplate.opsForHash().putAll(USER_TOKEN_PREFIX + userJSON.getOpenid() , stringObjectMap);
        stringRedisTemplate.expire(USER_TOKEN_PREFIX + userJSON.getOpenid() , USER_TOKEN_EXPIRE_TIME , TimeUnit.MINUTES) ;
        JwtService jwtService = new JwtService();
        String jwtOpenid = jwtService.generateToken(userJSON.getOpenid());
        userJSON.setJwtOpenId(jwtOpenid);
        userJSON.setUserName(passenger.getUsername());
        return Result.success(200,userJSON) ;
    }

    @Override
    public Result verifyRole(String verify, String stationId, String role, String vehicleId) {
//        String openid = UserHolder.getPerson().getOpenid();
        String openid = "1" ;
        switch (verify) {
            case "司机": {
                DriverCreateUser personAdapter = (DriverCreateUser) AdapterFactory.getPersonAdapter(Driver.class);
                personAdapter.createUser(openid, vehicleId);
                break;
            }
            case "供货员": {
                StationStaffCreateUser personAdapter = (StationStaffCreateUser) AdapterFactory.getPersonAdapter(StationStaff.class);
                personAdapter.createUser(openid, stationId, role);
                break;
            }
            case "平台管理人员": {
                PlatformSupervisorCreateUser personAdapter = (PlatformSupervisorCreateUser) AdapterFactory.getPersonAdapter(PlatformSupervisor.class);
                personAdapter.createUser(openid);
                break;
            }
        }
        return Result.success(200);
    }

    @Override
    public Result getUserCoupons() {
        String userId = UserHolder.getPerson().getOpenid();
        List<Coupon> coupons = couponMapper.selectList(
                new QueryWrapper<Coupon>().inSql("id",
                        "SELECT coupon_id FROM user_coupon WHERE user_id = " + userId)
        );
        return Result.success(200,coupons);
    }

    @Override
    public Result claimCoupon(String couponId) {
        SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator();
        UserCoupon userCoupon = UserCoupon.builder().id(String.valueOf(snowflakeIdGenerator.nextId()))
                .claimedAt(getTimeNow()).status(Constants.UNUSE)
                .couponId(couponId).userId(UserHolder.getPerson().getOpenid()).build();
        int result = userCouponMapper.insert(userCoupon);
        return result > 0 ? Result.success(200,"Coupon claimed successfully.") : Result.fail(400,"Failed to claim coupon.");
    }

    @Override
    public Result useCoupon(String couponId) {
        String userId = UserHolder.getPerson().getOpenid(); ;
        int result = couponMapper.update(
                null,
                new UpdateWrapper<Coupon>().set("status", "USED").eq("id", couponId)
        );
        return result > 0 ? Result.success(200,"Coupon used successfully.") : Result.fail(400,"Failed to use coupon.");
    }

    @Override
    public Result getCouponDetail(String couponId) {
        Coupon coupon = couponMapper.selectById(Long.parseLong(couponId));
        return Result.success(200,coupon);
    }

    @Override
    public Result getAllCoupons() {
        List<Coupon> coupons = couponMapper.selectList(null);
        return Result.success(200,coupons);
    }

    @Override
    public Result getUserProfile() {
        UserJSON userJSON = UserHolder.getPerson();
        if (userJSON.getRole().equals(PASSENGER)) {
            return Result.success(200,passengerMapper.selectById(userJSON.getOpenid())) ;
        } else if (userJSON.getRole().equals(DRIVER)) {
            return Result.success(200,driverMapper.selectById(userJSON.getOpenid())) ;
        } else if (userJSON.getRole().equals(STATIONSTAFF)) {
            return Result.success(200,stationStaffMapper.selectById(userJSON.getOpenid())) ;
        }
        return Result.success(200) ;
    }

    @Override
    public Result getUserOrderHistory() {
        List<OrderDriver> orders = orderMapper.selectList(new QueryWrapper<OrderDriver>().eq("passenger_id", UserHolder.getPerson().getOpenid()));
        return Result.success(200,orders);
    }

    @Override
    public Result getUserCurrentOrder() {
        List<OrderDriver> currentOrder = orderMapper.selectList(new QueryWrapper<OrderDriver>().eq("passenger_id", UserHolder.getPerson().getOpenid()).orderByDesc("order_date"));
        if (currentOrder != null) {
            return Result.success(200,currentOrder);
        } else {
            return Result.fail(200 , "No current order found for the user.");
        }
    }
}