package com.xiaomaotongzhi.randomBuy;

import com.xiaomaotongzhi.randomBuy.netty.networking.server.Server;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.stereotype.Component;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication(scanBasePackages = "com.xiaomaotongzhi.randomBuy")
@MapperScan("com.xiaomaotongzhi.randomBuy.mapper")
@EnableOpenApi
public class RandomBuyApplication {

    @Value("${netty.server.ip}")
    private static String ip ;

    @Value("${netty.server.ip}")
    private static Integer port ;

    public static void main(String[] args) {
        Server server = new Server() ;
        server.bind(ip , port);
        SpringApplication.run(RandomBuyApplication.class, args);
    }
}