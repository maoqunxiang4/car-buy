package com.xiaomaotongzhi.randomBuy.filter;

import cn.hutool.core.bean.BeanUtil;
import com.xiaomaotongzhi.randomBuy.utils.Constants;
import com.xiaomaotongzhi.randomBuy.utils.JwtService;
import com.xiaomaotongzhi.randomBuy.utils.UserHolder;
import com.xiaomaotongzhi.randomBuy.utils.UserJSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.xiaomaotongzhi.randomBuy.utils.Constants.*;

public class LoginInterceptor implements HandlerInterceptor {

    private StringRedisTemplate stringRedisTemplate ;

    public LoginInterceptor(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate ;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //检查它的role
        String token = request.getHeader(AUTHORIZATION);
        JwtService jwtService = new JwtService();
        String openid = jwtService.parseToken(token).getSubject();
        Map<Object, Object> map = stringRedisTemplate.opsForHash().entries(USER_TOKEN_PREFIX + openid);
        if (map.isEmpty()) {
            return false ;
        }
        UserJSON userJSON = new UserJSON();
        BeanUtil.fillBeanWithMap(map , userJSON , false) ;
        UserHolder.setPerson(userJSON);
        stringRedisTemplate.expire(USER_TOKEN_PREFIX + openid , USER_TOKEN_EXPIRE_TIME, TimeUnit.MINUTES) ;
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        UserHolder.removePerson();
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
