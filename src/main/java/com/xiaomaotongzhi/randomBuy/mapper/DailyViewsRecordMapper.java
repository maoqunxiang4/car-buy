package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyViewsRecord;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DailyViewsRecordMapper extends BaseMapper<DailyViewsRecord> {
}
