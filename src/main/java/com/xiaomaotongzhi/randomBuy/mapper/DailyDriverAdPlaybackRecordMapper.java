package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyDriverAdPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.utils.MapperBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DailyDriverAdPlaybackRecordMapper extends BaseMapper<DailyDriverAdPlaybackRecord> , MapperBean {
}
