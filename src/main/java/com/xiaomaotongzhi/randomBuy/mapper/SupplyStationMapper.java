package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Addition.SupplyStation;
import com.xiaomaotongzhi.randomBuy.utils.MapperBean;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SupplyStationMapper extends BaseMapper<SupplyStation>  , MapperBean {
}
