package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.UserBehaviorRecord;
import com.xiaomaotongzhi.randomBuy.utils.MapperBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserBehaviorRecordMapper extends BaseMapper<UserBehaviorRecord>  , MapperBean {
}
