package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.SupplyStationTotalSalesRecord;
import com.xiaomaotongzhi.randomBuy.utils.MapperBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SupplyStationTotalSalesRecordMapper extends BaseMapper<SupplyStationTotalSalesRecord>  , MapperBean {
}
