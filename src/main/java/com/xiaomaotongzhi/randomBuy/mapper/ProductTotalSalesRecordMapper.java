package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.ProductTotalSalesRecord;
import com.xiaomaotongzhi.randomBuy.utils.MapperBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProductTotalSalesRecordMapper extends BaseMapper<ProductTotalSalesRecord>  , MapperBean {
}
