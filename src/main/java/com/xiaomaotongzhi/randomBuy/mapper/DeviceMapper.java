package com.xiaomaotongzhi.randomBuy.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Device;
import com.xiaomaotongzhi.randomBuy.utils.MapperBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DeviceMapper extends BaseMapper<Device> , MapperBean {
}
