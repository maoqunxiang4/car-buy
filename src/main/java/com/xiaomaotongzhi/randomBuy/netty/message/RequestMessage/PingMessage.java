package com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage;

import com.xiaomaotongzhi.randomBuy.netty.message.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.xiaomaotongzhi.randomBuy.netty.message.MessageType.PING_REQUEST_MESSAGE;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PingMessage extends Message {
    private String message ;

    @Override
    public Integer getMessageType() {
        return PING_REQUEST_MESSAGE;
    }
}
