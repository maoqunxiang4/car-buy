package com.xiaomaotongzhi.randomBuy.netty.message;

public interface MessageType {
    Integer PRODUCT_REQUEST_MESSAGE = 1 ;
    Integer PRODUCT_RESPONSE_MESSAGE = 2 ;
    Integer PING_REQUEST_MESSAGE = 3 ;
    Integer PONG_RESPONSE_MESSAGE = 4 ;
}
