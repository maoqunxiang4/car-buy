package com.xiaomaotongzhi.randomBuy.netty.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractResponseMessage extends Message {
    protected Integer code ;
    protected String message ;

}
