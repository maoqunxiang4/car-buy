package com.xiaomaotongzhi.randomBuy.netty.message.ResponseMessage;

import com.xiaomaotongzhi.randomBuy.netty.message.AbstractResponseMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.xiaomaotongzhi.randomBuy.netty.message.MessageType.PONG_RESPONSE_MESSAGE;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PongMessage extends AbstractResponseMessage {
    private String message ;

    @Override
    public Integer getMessageType() {
        return PONG_RESPONSE_MESSAGE;
    }
}
