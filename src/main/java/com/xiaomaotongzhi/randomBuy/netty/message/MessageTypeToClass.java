package com.xiaomaotongzhi.randomBuy.netty.message;



import com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage.PingMessage;
import com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage.ProductRequestMessage;
import com.xiaomaotongzhi.randomBuy.netty.message.ResponseMessage.PongMessage;
import com.xiaomaotongzhi.randomBuy.netty.message.ResponseMessage.ProductResponseMessage;

import java.util.HashMap;

import static com.xiaomaotongzhi.randomBuy.netty.message.MessageType.*;


public class MessageTypeToClass {
    private static final HashMap<Integer , Class<? extends Message>> typeMap = new HashMap<>();

    static {
        typeMap.put(PRODUCT_REQUEST_MESSAGE , ProductRequestMessage.class ) ;
        typeMap.put(PRODUCT_RESPONSE_MESSAGE , ProductResponseMessage.class ) ;
        typeMap.put(PING_REQUEST_MESSAGE , PingMessage.class ) ;
        typeMap.put(PONG_RESPONSE_MESSAGE , PongMessage.class ) ;
    }

    public static Class<? extends Message> getMessageType(Integer message) {
        return typeMap.get(message) ;
    }
}
