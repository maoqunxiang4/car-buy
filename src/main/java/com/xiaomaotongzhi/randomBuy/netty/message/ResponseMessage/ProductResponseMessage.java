package com.xiaomaotongzhi.randomBuy.netty.message.ResponseMessage;

import com.xiaomaotongzhi.randomBuy.netty.message.AbstractResponseMessage;
import com.xiaomaotongzhi.randomBuy.netty.message.MessageType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductResponseMessage extends AbstractResponseMessage {
    private String productId ;
    private String driverId ;
    private Integer isSale ;

    public ProductResponseMessage(String productId , String driverId , Integer isSale ) {
        this.productId = productId ;
        this.driverId = driverId ;
        this.isSale = isSale ;
    }

    public ProductResponseMessage(Integer code ,String message) {
        super(code,message);
    }

    @Override
    public Integer getMessageType() {
        return MessageType.PRODUCT_RESPONSE_MESSAGE;
    }
}
