package com.xiaomaotongzhi.randomBuy.netty.message;

import lombok.Data;

@Data
public abstract class Message {
    public abstract Integer getMessageType() ;
}
