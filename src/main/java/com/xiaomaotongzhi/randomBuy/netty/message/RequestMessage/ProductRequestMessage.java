package com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage;

import com.xiaomaotongzhi.randomBuy.netty.message.Message;
import com.xiaomaotongzhi.randomBuy.netty.message.MessageType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequestMessage extends Message {
    private String productId ;
    private String driverId ;
    private Integer isSale ;

    @Override
    public Integer getMessageType() {
        return MessageType.PRODUCT_REQUEST_MESSAGE;
    }
}
