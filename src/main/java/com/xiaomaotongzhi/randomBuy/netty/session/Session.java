package com.xiaomaotongzhi.randomBuy.netty.session;

import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

public class Session {
    private static final Map<String , Channel> usernameChannelMap = new HashMap<>();
    private static final Map<Channel , String> channelUsernameMap = new HashMap<>();

    public void bind(Channel channel , String username){
        usernameChannelMap.put(username, channel) ;
        channelUsernameMap.put(channel , username) ;
    }

    public void unbind(Channel channel , String username) {
        String userName = channelUsernameMap.remove(channel);
        if (userName!=null) {
            usernameChannelMap.remove(username) ;
        }
    }

    public Channel getChannel(String username) {
        return usernameChannelMap.get(username) ;
    }

}
