package com.xiaomaotongzhi.randomBuy.netty.networking.client;

import com.xiaomaotongzhi.randomBuy.netty.codec.RandomBuyByteToMessageDecoder;
import com.xiaomaotongzhi.randomBuy.netty.codec.RandomBuyMessageToByteEncoder;
import com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage.PingMessage;
import com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage.ProductRequestMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoop;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Scanner;

@Slf4j
public class RandomBuyClientChannelInitializer extends ChannelInitializer<NioSocketChannel> {
    private final Scanner scanner = new Scanner(System.in) ;
    private final LoggingHandler loggingHandler = new LoggingHandler();
    private final RandomBuyMessageToByteEncoder randomBuyMessageToByteEncoder = new RandomBuyMessageToByteEncoder() ;
    private final RandomBuyByteToMessageDecoder randomBuyByteToMessageDecoder = new RandomBuyByteToMessageDecoder() ;

    @Override
    protected void initChannel(NioSocketChannel ch) throws Exception {
        ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(1024 , 12 , 4 , 0 ,0)) ;
        ch.pipeline().addLast(loggingHandler) ;
        ch.pipeline().addLast(randomBuyMessageToByteEncoder) ;
        ch.pipeline().addLast(randomBuyByteToMessageDecoder) ;
        ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
            @Override
            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                super.channelRead(ctx, msg);
            }

            @Override
            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                super.channelActive(ctx);
                ctx.writeAndFlush(new ProductRequestMessage("product1" , "driver1" , 1)) ;
            }
        }) ;
        ch.pipeline().addLast(new IdleStateHandler(0,3,0)) ;
        ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
            @Override
            public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
                super.userEventTriggered(ctx, evt);
                IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
                if (idleStateEvent.state() == IdleState.WRITER_IDLE) {
                    log.debug("客户端已经3s没有写数据了");
                    ctx.writeAndFlush(new PingMessage("你好，我是客户端")) ;
                }
            }
        }) ;
        ch.pipeline().addLast(new IdleStateHandler(8,0,0)) ;
        ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
            @Override
            public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
                super.userEventTriggered(ctx, evt);
                IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
                if (idleStateEvent.state() == IdleState.READER_IDLE) {
                    log.debug("客户端已经很久没有接收数据了");
                    //当遇到读空闲时，关闭Channel
                    ctx.channel().close() ;
                }
            }
        }) ;
        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
            @Override
            public void channelInactive(ChannelHandlerContext ctx) throws Exception {
                super.channelInactive(ctx);
                EventLoop eventLoop = ctx.channel().eventLoop();
                eventLoop.submit(() -> {
                    log.debug("开始连接");
                    Client client = new Client();
                    client.connect("127.0.0.1", 8000);
                }) ;
            }

            @Override
            public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
                log.debug("连接已断开");
                super.exceptionCaught(ctx, cause);
            }

        }) ;
    }
}