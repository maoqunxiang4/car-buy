package com.xiaomaotongzhi.randomBuy.netty.networking.server;

import com.xiaomaotongzhi.randomBuy.netty.codec.RandomBuyByteToMessageDecoder;
import com.xiaomaotongzhi.randomBuy.netty.codec.RandomBuyMessageToByteEncoder;
import com.xiaomaotongzhi.randomBuy.netty.handler.ProductRequestHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RandomBuyServerChannelInitializer extends ChannelInitializer<NioSocketChannel> {
    private final LoggingHandler loggingHandler = new LoggingHandler() ;
    private final RandomBuyMessageToByteEncoder randomBuyMessageToByteEncoder = new RandomBuyMessageToByteEncoder() ;
    private final RandomBuyByteToMessageDecoder randomBuyByteToMessageDecoder = new RandomBuyByteToMessageDecoder() ;
    private final ProductRequestHandler productRequestHandler = new ProductRequestHandler() ;

    @Override
    protected void initChannel(NioSocketChannel ch) throws Exception {
        ch.pipeline().addLast(new LengthFieldBasedFrameDecoder(1024 , 12 , 4 , 0 ,0)) ;
        ch.pipeline().addLast(loggingHandler) ;
        ch.pipeline().addLast(randomBuyMessageToByteEncoder) ;
        ch.pipeline().addLast(randomBuyByteToMessageDecoder) ;
        ch.pipeline().addLast(productRequestHandler) ;
        ch.pipeline().addLast(new IdleStateHandler(0,3,0)) ;
        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){
            @Override
            public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
                super.userEventTriggered(ctx, evt);
                IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
                if (idleStateEvent.state() == IdleState.WRITER_IDLE) {
                    log.debug("3s没有写数据了，发送一个心跳包");
//                    ctx.writeAndFlush(new PongMessage("你好,我是服务端")) ;
                }
            }
        }) ;
        ch.pipeline().addLast(new ChannelInboundHandlerAdapter(){

        }) ;
    }
}
