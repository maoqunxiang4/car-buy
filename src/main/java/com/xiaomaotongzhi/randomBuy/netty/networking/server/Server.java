package com.xiaomaotongzhi.randomBuy.netty.networking.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

@Slf4j
public class Server {

    private final ServerBootstrap serverBootstrap = new ServerBootstrap();
    private final NioEventLoopGroup boss = new NioEventLoopGroup(1);
    private final NioEventLoopGroup worker = new NioEventLoopGroup();

    public Server() {
        serverBootstrap.group(boss , worker) ;
        serverBootstrap.channel(NioServerSocketChannel.class) ;
        serverBootstrap.childHandler(new RandomBuyServerChannelInitializer()) ;
    }

    public void bind(String ip , Integer port)  {
        try {
            Channel channel = serverBootstrap.bind(new InetSocketAddress(ip, port)).sync().channel();
            channel.closeFuture().addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    if (channelFuture.isSuccess()) {
                        boss.shutdownGracefully() ;
                        worker.shutdownGracefully() ;
                    }
                }
            }) ;
        } catch (Exception e) {
            log.debug("Exception:" + e.getMessage());
        }

    }

}
