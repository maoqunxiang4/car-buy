package com.xiaomaotongzhi.randomBuy.netty.networking.client;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class Client {

    private Bootstrap bootstrap = new Bootstrap() ;
    private NioEventLoopGroup eventLoopGroup = new NioEventLoopGroup() ;
    private AtomicInteger retryTime = new AtomicInteger(1) ;

    public Client() {
        bootstrap.channel(NioSocketChannel.class) ;
        bootstrap.group(eventLoopGroup) ;
        bootstrap.handler(new RandomBuyClientChannelInitializer()) ;
    }

    public void connect(String ip , Integer port) {
        ChannelFuture connect = bootstrap.connect(ip, port);
        connect.addListener(new ChannelFutureListener(){
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                if (!channelFuture.isSuccess()) {
                    if (retryTime.get() != 3){
                        connect(ip, port) ;
                        retryTime.getAndIncrement() ;
                    } else {
                        log.debug("error");
                    }
                } else {
                    Channel channel = connect.channel();
                    channel.closeFuture().addListener(new ChannelFutureListener(){
                        @Override
                        public void operationComplete(ChannelFuture channelFuture) throws Exception {
                            if (channelFuture.isSuccess()) {
                                eventLoopGroup.shutdownGracefully();
                            }
                        }
                    }) ;
                }
            }
        }) ;
    }
}
