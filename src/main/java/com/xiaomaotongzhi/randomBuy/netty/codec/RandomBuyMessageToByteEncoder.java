package com.xiaomaotongzhi.randomBuy.netty.codec;

import cn.hutool.json.JSONUtil;
import com.xiaomaotongzhi.randomBuy.netty.message.Message;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

@ChannelHandler.Sharable
@Slf4j
public class RandomBuyMessageToByteEncoder extends MessageToMessageEncoder<Message> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Message msg, List<Object> list) throws Exception {
        //获取Json字符串
        String jsonStr = JSONUtil.toJsonStr(msg);
        ByteBuf byteBuf = ByteBufAllocator.DEFAULT.buffer();
        //幻数
        byteBuf.writeBytes(new byte[]{'r' , 'a' , 'n' , 'd' , 'o' , 'm' , 'b' , 'u' , 'y' }) ;

        //协议版本
        byteBuf.writeByte(1) ;

        //序列化方式
        byteBuf.writeByte(1) ;

        //功能指令
        byteBuf.writeByte(msg.getMessageType()) ;

        //正文长度
        byteBuf.writeInt(jsonStr.getBytes(StandardCharsets.UTF_8).length) ;

        //正文
        byteBuf.writeCharSequence(jsonStr , Charset.defaultCharset()) ;

        list.add(byteBuf) ;
    }
}
