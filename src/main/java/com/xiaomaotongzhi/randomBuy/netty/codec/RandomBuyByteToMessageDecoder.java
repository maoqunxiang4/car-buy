package com.xiaomaotongzhi.randomBuy.netty.codec;

import cn.hutool.json.JSONUtil;
import com.xiaomaotongzhi.randomBuy.netty.message.Message;
import com.xiaomaotongzhi.randomBuy.netty.message.MessageTypeToClass;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import lombok.extern.slf4j.Slf4j;


import java.nio.charset.Charset;
import java.util.List;

@ChannelHandler.Sharable
@Slf4j
public class RandomBuyByteToMessageDecoder extends MessageToMessageDecoder<ByteBuf> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> list) throws Exception {
        //幻数 9个字节
        ByteBuf magicNumber = byteBuf.readBytes(9);
        log.debug("magicNumber: " + magicNumber);

        //协议版本号
        byte protoVersion = byteBuf.readByte();
        log.debug("protoVersion: " + protoVersion);

        //序列化方式
        byte serializableNo = byteBuf.readByte();
        log.debug("serializableNo: " + serializableNo);

        //功能指令
        byte funNo = byteBuf.readByte();
        log.debug("funNo: " + funNo);

        //正文长度
        int contentLength = byteBuf.readInt();
        log.debug("contentLength: " + contentLength);

        //正文
        Message message = null ;
        if (serializableNo == 1) {
            Class<? extends Message> messageType = MessageTypeToClass.getMessageType((int) funNo);
            message = JSONUtil.toBean(byteBuf.readCharSequence(contentLength, Charset.defaultCharset()).toString(),
                    messageType);
        }
        log.debug("message: " + message);

        list.add(message) ;
    }
}