package com.xiaomaotongzhi.randomBuy.netty.handler;

import com.xiaomaotongzhi.randomBuy.netty.message.RequestMessage.ProductRequestMessage;
import com.xiaomaotongzhi.randomBuy.netty.message.ResponseMessage.ProductResponseMessage;
import com.xiaomaotongzhi.randomBuy.netty.session.Session;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;



@ChannelHandler.Sharable
@Slf4j
public class ProductRequestHandler extends SimpleChannelInboundHandler<ProductRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ProductRequestMessage productRequestMessage) throws Exception {
        String productId = productRequestMessage.getProductId() ;
        String driverId = productRequestMessage.getDriverId() ;
        Integer isSale = productRequestMessage.getIsSale() ;

        Session session = new Session() ;
        String username = "123" ;
        Channel channel = session.getChannel(username) ;

        if (channel!=null) {
            channel.writeAndFlush(new ProductResponseMessage(productId , driverId , isSale)) ;
        } else {
            ctx.writeAndFlush(new ProductResponseMessage(400 , "连接有误")) ;
        }
    }
}
