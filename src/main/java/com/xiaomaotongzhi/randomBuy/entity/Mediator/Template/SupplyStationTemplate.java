package com.xiaomaotongzhi.randomBuy.entity.Mediator.Template;

import com.xiaomaotongzhi.randomBuy.entity.Addition.OrderDriver;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.SupplyStationDailySalesRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.SupplyStationTotalSalesRecord;
import com.xiaomaotongzhi.randomBuy.mapper.OrderDriverMapper;
import com.xiaomaotongzhi.randomBuy.mapper.SupplyStationDailySalesRecordMapper;
import com.xiaomaotongzhi.randomBuy.mapper.SupplyStationTotalSalesRecordMapper;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.UUID;

import static com.xiaomaotongzhi.randomBuy.utils.Constants.VALUE_UNIT;
import static com.xiaomaotongzhi.randomBuy.utils.MapKey.*;
import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNow;

@Component
public class SupplyStationTemplate extends AdditionTemplate implements TemplateBean{
    @Override
    public void createOrder(HashMap<String,String> map) {
        OrderDriver order = additionDirecter
                .buildOrder(map.get(PRODUCT_ID), map.get(PASSENGER_ID), map.get(DRIVER_ID)
                        , LocalDateTime.parse(map.get(ORDER_DATE) , DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss")),
                        Double.parseDouble(map.get(AMOUNT)), map.get(VEHICLE_ID) , Double.parseDouble(map.get(ORDER_PRICE)));
        OrderDriverMapper orderMapper = (OrderDriverMapper) mapperFactory.getToolHandler("orderMapper");
        orderMapper.insert(order) ;
    }

    @Override
    public void recordBehavior(HashMap<String,String> map) {
        double salePrice = Double.parseDouble(map.get(SALE_PRICE)) * VALUE_UNIT;  // Convert from "wan" to actual value

        //补货站日记录 SupplyStationDailySales
        SupplyStationDailySalesRecordMapper supplyStationDailySalesMapper = (SupplyStationDailySalesRecordMapper) mapperFactory.getToolHandler("supplyStationDailySalesMapper");
        SupplyStationDailySalesRecord supplyStationDailySales = supplyStationDailySalesMapper.selectById(map.get(PRODUCT_ID));
        if (supplyStationDailySales == null) {
            supplyStationDailySales = new SupplyStationDailySalesRecord();
            supplyStationDailySales.setId(UUID.randomUUID().toString());  // Generate a new ID
            supplyStationDailySales.setDate(getTimeNow());  // Set the current date
            supplyStationDailySales.setProductId(map.get(PRODUCT_ID));
            supplyStationDailySales.setProductValue(salePrice / VALUE_UNIT);  // Convert back to "wan" unit
            supplyStationDailySales.setDailyRevenue(salePrice / VALUE_UNIT);  // Convert back to "wan" unit
            supplyStationDailySalesMapper.insert(supplyStationDailySales);
        } else {
            supplyStationDailySales.setProductValue(supplyStationDailySales.getProductValue() + salePrice / VALUE_UNIT);  // Convert back to "wan" unit
            supplyStationDailySales.setDailyRevenue(supplyStationDailySales.getDailyRevenue() + salePrice / VALUE_UNIT);  // Convert back to "wan" unit
            supplyStationDailySalesMapper.updateById(supplyStationDailySales);
        }

        //补货站总体记录 SupplyStationTotalSalesRecord
        SupplyStationTotalSalesRecordMapper supplyStationTotalSalesRecordMapper = (SupplyStationTotalSalesRecordMapper) mapperFactory.getToolHandler("supplyStationTotalSalesRecordMapper");
        SupplyStationTotalSalesRecord supplyStationTotalSalesRecord = supplyStationTotalSalesRecordMapper.selectById(map.get("station_id"));
        if (supplyStationTotalSalesRecord == null) {
            supplyStationTotalSalesRecord = new SupplyStationTotalSalesRecord();
            supplyStationTotalSalesRecord.setId(UUID.randomUUID().toString());  // Generate a new ID
            supplyStationTotalSalesRecord.setStationId(map.get(STATION_ID));
            supplyStationTotalSalesRecord.setTotalRevenue(salePrice / VALUE_UNIT);  // Convert back to "wan" unit
            supplyStationTotalSalesRecordMapper.insert(supplyStationTotalSalesRecord);
        } else {
            supplyStationTotalSalesRecord.setTotalRevenue(supplyStationTotalSalesRecord.getTotalRevenue() + salePrice / VALUE_UNIT);  // Convert back to "wan" unit
            supplyStationTotalSalesRecordMapper.updateById(supplyStationTotalSalesRecord);
        }
    }

}
