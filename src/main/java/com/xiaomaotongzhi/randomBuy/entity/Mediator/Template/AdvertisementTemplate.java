package com.xiaomaotongzhi.randomBuy.entity.Mediator.Template;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Advertisement;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyDriverAdPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.AdTotalPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.mapper.AdTotalPlaybackRecordMapper;
import com.xiaomaotongzhi.randomBuy.mapper.AdvertisementMapper;
import com.xiaomaotongzhi.randomBuy.mapper.DailyDriverAdPlaybackRecordMapper;
import com.xiaomaotongzhi.randomBuy.utils.Constants;
import com.xiaomaotongzhi.randomBuy.utils.SnowflakeIdGenerator;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import static com.xiaomaotongzhi.randomBuy.utils.Constants.DATE_FORMATTER;
import static com.xiaomaotongzhi.randomBuy.utils.Constants.VALUE_UNIT;
import static com.xiaomaotongzhi.randomBuy.utils.MapKey.*;
import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNow;

@Component
public class AdvertisementTemplate extends AdditionTemplate implements TemplateBean{

    @Override
    public void createOrder(HashMap<String,String> map) {
        Advertisement advertisement = additionDirecter.buildAdvertisement(
                map.get(ADVERTISEMENT_ID),
                map.get(ADVERTISEMENT_TITLE),
                map.get(ADVERTISEMENT_CONTENT),
                map.get(ADVERTISEMENT_VERTISER),
                LocalDateTime.parse(map.get(ADVERTISEMENT_STARTDATE)),
                LocalDateTime.parse(map.get(ADVERTISEMENT_ENDDATE)),
                map.get(ADVERTISEMENT_TARGETAUDIENCE)
        );

        AdvertisementMapper advertisementMapper = (AdvertisementMapper) mapperFactory.getToolHandler("advertisementMapper");
        advertisementMapper.insert(advertisement);
    }

    @Override
    public void recordBehavior(HashMap<String,String> map) {
        // 对总播放量产生影响
        AdTotalPlaybackRecordMapper adTotalPlaybackRecordMapper = (AdTotalPlaybackRecordMapper) mapperFactory.getToolHandler("adTotalPlaybackRecordMapper");

        AdTotalPlaybackRecord adTotalPlaybackRecord = adTotalPlaybackRecordMapper.selectOne(new QueryWrapper<AdTotalPlaybackRecord>()
                .eq("addition_id" , map.get(ADVERTISEMENT_TOTAL_ID)));
        SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator();

        if (adTotalPlaybackRecord != null) {
            double currentTotalNumber = adTotalPlaybackRecord.getTotalNumber() ;  // Convert from "wan" to actual value
            adTotalPlaybackRecord.setTotalNumber((currentTotalNumber + 1));  // Convert back to "wan" unit after incrementing
            adTotalPlaybackRecordMapper.updateById(adTotalPlaybackRecord);
        } else {
            adTotalPlaybackRecord = new AdTotalPlaybackRecord();
            adTotalPlaybackRecord.setId(String.valueOf(snowflakeIdGenerator.nextId()));
            adTotalPlaybackRecord.setAdditionId(map.get(ADVERTISEMENT_TOTAL_ID));
            adTotalPlaybackRecord.setTotalNumber(1.0 / VALUE_UNIT);  // Initialize with 1 view in "wan" unit
            adTotalPlaybackRecordMapper.insert(adTotalPlaybackRecord);
        }

        // 对单日播放量产生影响
        DailyDriverAdPlaybackRecordMapper dailyDriverAdPlaybackRecordMapper = (DailyDriverAdPlaybackRecordMapper) mapperFactory.getToolHandler("dailyDriverAdPlaybackRecordMapper");
        DailyDriverAdPlaybackRecord dailyDriverAdPlaybackRecord =
                dailyDriverAdPlaybackRecordMapper.selectOne(new QueryWrapper<DailyDriverAdPlaybackRecord>().eq("ad_id" , map.get(ADVERTISEMENT_DRIVER_DAILY_ID)));

        if (dailyDriverAdPlaybackRecord != null) {
            double currentDailyViews = dailyDriverAdPlaybackRecord.getNumberOfViews() ;  // Convert from "wan" to actual value
            dailyDriverAdPlaybackRecord.setNumberOfViews(currentDailyViews + 1);  // Convert back to "wan" unit after incrementing
            dailyDriverAdPlaybackRecordMapper.updateById(dailyDriverAdPlaybackRecord);
        } else {
            dailyDriverAdPlaybackRecord = new DailyDriverAdPlaybackRecord();
            dailyDriverAdPlaybackRecord.setId(String.valueOf(snowflakeIdGenerator.nextId()));
            dailyDriverAdPlaybackRecord.setDate(getTimeNow());
            dailyDriverAdPlaybackRecord.setDriverId(map.get(DRIVER_ID));
            dailyDriverAdPlaybackRecord.setAdId(map.get(ADVERTISEMENT_DRIVER_DAILY_ID));
            dailyDriverAdPlaybackRecord.setNumberOfViews(1.0);  // Initialize with 1 view in "wan" unit
            dailyDriverAdPlaybackRecordMapper.insert(dailyDriverAdPlaybackRecord);
        }
    }

}
