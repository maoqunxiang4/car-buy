package com.xiaomaotongzhi.randomBuy.entity.Mediator.Template;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("templateFactory")
public class TemplateFactory implements ApplicationContextAware , InitializingBean {
    private Map<String , TemplateBean> templateBeanMap ;
    private ApplicationContext applicationContext ;

    @Override
    public void afterPropertiesSet() throws Exception {
        final Map<String , TemplateBean> templateBeanMap = applicationContext.getBeansOfType(TemplateBean.class) ;
        this.templateBeanMap = templateBeanMap ;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext ;
    }

    public TemplateBean getTemplateBean(String templateBeanName) {
        return templateBeanMap.get(templateBeanName);
    }
}
