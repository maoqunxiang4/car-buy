package com.xiaomaotongzhi.randomBuy.entity.Mediator.Template;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaotongzhi.randomBuy.entity.Addition.OrderDriver;
import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailySalesRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.SalesRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.UserBehaviorRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.ProductTotalSalesRecord;
import com.xiaomaotongzhi.randomBuy.mapper.*;
import com.xiaomaotongzhi.randomBuy.utils.UserHolder;
import org.springframework.stereotype.Component;

import java.util.HashMap;

import static com.xiaomaotongzhi.randomBuy.utils.Constants.VALUE_UNIT;
import static com.xiaomaotongzhi.randomBuy.utils.MapKey.*;
import static com.xiaomaotongzhi.randomBuy.utils.UsualUtils.getTimeNow;

@Component
public class ProductTemplate extends AdditionTemplate implements TemplateBean{
    @Override
    public void createOrder(HashMap<String,String> map) {
        OrderDriver order = additionDirecter
                .buildOrder(map.get(PRODUCT_ID), map.get(PASSENGER_ID), map.get(DRIVER_ID)
                        , getTimeNow(), Double.parseDouble(map.get(AMOUNT)), map.get(VEHICLE_ID) , Double.parseDouble(map.get(ORDER_PRICE)));
        OrderDriverMapper orderMapper = (OrderDriverMapper) mapperFactory.getToolHandler("orderMapper");
        orderMapper.insert(order) ;
    }

    @Override
    public void recordBehavior(HashMap<String,String> map) {
        //记录当日销售量 DailySalesRecord
        DailySalesRecordMapper dailySalesRecordMapper = (DailySalesRecordMapper) mapperFactory.getToolHandler("dailySalesRecordMapper");
        DailySalesRecord dailySalesRecord =
                dailySalesRecordMapper.selectOne(new QueryWrapper<DailySalesRecord>().eq("product_id",map.get(PRODUCT_ID)));
        double salePrice = Double.parseDouble(map.get(SALE_PRICE)) * VALUE_UNIT;  // Convert from "wan" to actual value
        if (dailySalesRecord == null) {
            dailySalesRecord = (DailySalesRecord) recordStore.getRecord(DailySalesRecord.class);
            dailySalesRecord.setDate(getTimeNow());
            dailySalesRecord.setProductId(map.get(PRODUCT_ID));
            dailySalesRecord.setDailySalesAmount(salePrice);
            dailySalesRecordMapper.insert(dailySalesRecord);
        } else {
            dailySalesRecord.setDate(getTimeNow());
            dailySalesRecord.setDailySalesAmount(dailySalesRecord.getDailySalesAmount() + salePrice);
            dailySalesRecordMapper.updateById(dailySalesRecord);
        }

        //记录销售记录 SalesRecord
        SalesRecordMapper salesRecordMapper = (SalesRecordMapper) mapperFactory.getToolHandler("salesRecordMapper");
        SalesRecord salesRecord = (SalesRecord) recordStore.getRecord(SalesRecord.class);
        salesRecord.setDriverId(map.get(DRIVER_ID));
        salesRecord.setPassengerId(map.get(PASSENGER_ID));
        salesRecord.setSaleDate(getTimeNow());
        salesRecord.setSalePrice(salePrice);  // Convert back to "wan" unit
        salesRecord.setX(Double.parseDouble(map.get(X)));
        salesRecord.setY(Double.parseDouble(map.get(Y)));
        salesRecord.setId(map.get(SALES_RECORD_ID));
        salesRecordMapper.insert(salesRecord);

        //记录用户行为 UserBehaviorRecord
        UserBehaviorRecordMapper userBehaviorRecordMapper = (UserBehaviorRecordMapper) mapperFactory.getToolHandler("userBehaviorRecordMapper");
        UserBehaviorRecord userBehaviorRecord = (UserBehaviorRecord) recordStore.getRecord(UserBehaviorRecord.class);
        userBehaviorRecord.setId(map.get(USER_BEHAVIOR_RECORD_ID));
        userBehaviorRecord.setUserId(UserHolder.getPerson().getOpenid());
        userBehaviorRecord.setProductId(map.get(PRODUCT_ID));
        userBehaviorRecord.setBehaviorType(PURCHASE_OVER);
        userBehaviorRecord.setBehaviorDate(getTimeNow());
        userBehaviorRecord.setX(Double.parseDouble(map.get(X)));
        userBehaviorRecord.setY(Double.parseDouble(map.get(Y)));
        userBehaviorRecordMapper.insert(userBehaviorRecord);

        //记录总体销售记录 ProductTotalSalesRecord
        ProductTotalSalesRecordMapper productTotalSalesRecordMapper = (ProductTotalSalesRecordMapper) mapperFactory.getToolHandler("productTotalSalesRecordMapper");
        DriverProductMapper driverProductMapper = (DriverProductMapper) mapperFactory.getToolHandler("productMapper");
        ProductTotalSalesRecord productTotalSalesRecord = productTotalSalesRecordMapper.selectById(map.get(PRODUCT_ID));
        if (productTotalSalesRecord == null) {
            productTotalSalesRecord = (ProductTotalSalesRecord) recordStore.getRecord(ProductTotalSalesRecord.class) ;
            productTotalSalesRecord.setTotalRevenue(salePrice);  // Convert back to "wan" unit
            productTotalSalesRecord.setAveragePrice(salePrice);  // Convert back to "wan" unit
            DriverProduct driverProduct = driverProductMapper.selectById(map.get(PRODUCT_ID));
            productTotalSalesRecord.setId(driverProduct.getId());
            productTotalSalesRecord.setTotalNumber(1 / VALUE_UNIT); // Convert back to "wan"
            productTotalSalesRecord.setImgUrl(driverProduct.getImageUrl());
            productTotalSalesRecord.setAdditionId(map.get(PRODUCT_ID));
            productTotalSalesRecordMapper.insert(productTotalSalesRecord);
        } else {
            double totalRevenue = productTotalSalesRecord.getTotalRevenue() * VALUE_UNIT + salePrice;  // Convert from "wan" to actual value
            productTotalSalesRecord.setTotalRevenue(totalRevenue / VALUE_UNIT);  // Convert back to "wan" unit
            double averagePrice = totalRevenue / (productTotalSalesRecord.getAveragePrice() * VALUE_UNIT + 1);  // Convert from "wan" to actual value
            productTotalSalesRecord.setAveragePrice(averagePrice / VALUE_UNIT);  // Convert back to "wan" unit
            DriverProduct driverProduct = driverProductMapper.selectById(map.get(PRODUCT_ID));
            productTotalSalesRecord.setId(driverProduct.getId());
            productTotalSalesRecord.setTotalNumber(productTotalSalesRecord.getTotalNumber() * VALUE_UNIT + 1); // Convert back to "wan"
            productTotalSalesRecord.setImgUrl(driverProduct.getImageUrl());
            productTotalSalesRecordMapper.updateById(productTotalSalesRecord);
        }
    }
}
