package com.xiaomaotongzhi.randomBuy.entity.Mediator.Template;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Builder.AdditionDirecter;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.RecordFactory.RecordStore;
import com.xiaomaotongzhi.randomBuy.utils.MapperFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public abstract class AdditionTemplate {
    protected AdditionDirecter additionDirecter = AdditionDirecter.getInstance() ;

    protected RecordStore recordStore = RecordStore.getInstance() ;

    protected MapperFactory mapperFactory = MapperFactory.getInstance() ;

    public final void additionDoingTemplate(HashMap<String,String> map) {
        //生成订单
        createOrder(map);
        //记录相关的行为
        recordBehavior(map);
    }

    public final void additionDoingTemplateOnlyOne(HashMap<String,String> map) {
        //记录相关的行为
        recordBehavior(map);
    }

    public abstract void createOrder(HashMap<String,String> map);
    public abstract void recordBehavior(HashMap<String,String> map);
}
