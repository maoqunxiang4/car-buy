package com.xiaomaotongzhi.randomBuy.entity.Mediator;

import lombok.Data;

@Data
public class ProductStrategy implements Strategy {
    //在创建了一个商品之后，会引起哪些问题
    @Override
    public void afterDoing() {
        //生成订单

        //用户,添加购买记录

        //司机,添加出售记录

        //后台,添加商品相关信息

    }
}
