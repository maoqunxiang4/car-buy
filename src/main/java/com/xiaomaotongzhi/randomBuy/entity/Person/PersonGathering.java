package com.xiaomaotongzhi.randomBuy.entity.Person;

public class PersonGathering {

    public PersonGathering() {}

    private static final class PersonGatheringHolder {
        private static final PersonGathering personGathering = new PersonGathering() ;
    }

    public static PersonGathering getInstance() {
        return PersonGatheringHolder.personGathering ;
    }

    public Person getPerson(Class<? extends Person> clazz ) {
        if (clazz.equals(Passenger.class)) {
            return new PassengerFactory().createPerson() ;
        } else if (clazz.equals(StationStaff.class)) {
            return new StationStaffFactory().createPerson() ;
        } else if (clazz.equals(Driver.class)) {
            return new DriverFactory().createPerson() ;
        } else if (clazz.equals(PlatformSupervisor.class)) {
            return new PlatformSupervisorFactory().createPerson() ;
        }
        return null ;
    }
}
