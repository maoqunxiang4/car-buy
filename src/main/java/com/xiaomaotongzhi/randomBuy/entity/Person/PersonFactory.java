package com.xiaomaotongzhi.randomBuy.entity.Person;

import java.lang.reflect.Type;

public interface PersonFactory {
    Person createPerson() ;
}
