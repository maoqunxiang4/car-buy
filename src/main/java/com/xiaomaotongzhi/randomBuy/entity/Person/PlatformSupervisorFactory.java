package com.xiaomaotongzhi.randomBuy.entity.Person;

public class PlatformSupervisorFactory implements PersonFactory{
    @Override
    public Person createPerson() {
        return new PlatformSupervisor();
    }
}
