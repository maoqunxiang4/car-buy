package com.xiaomaotongzhi.randomBuy.entity.Person;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaomaotongzhi.randomBuy.entity.FeedBack.DriverFeedback;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;

@TableName("station_staff")
public class StationStaff extends Person implements ObjectBean {
    @TableField("station_id")
    private String stationId;
    private String role;

    public StationStaff() {
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public void feedBack() {
        if (feedback instanceof DriverFeedback) {
            DriverFeedback driverFeedback = (DriverFeedback) feedback;
            //插入
        }
    }
}
