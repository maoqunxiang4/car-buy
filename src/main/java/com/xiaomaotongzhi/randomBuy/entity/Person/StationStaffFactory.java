package com.xiaomaotongzhi.randomBuy.entity.Person;

public class StationStaffFactory implements PersonFactory {

    @Override
    public Person createPerson() {
        return new StationStaff();
    }
}
