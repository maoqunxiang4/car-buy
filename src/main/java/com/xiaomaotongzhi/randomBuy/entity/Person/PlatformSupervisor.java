package com.xiaomaotongzhi.randomBuy.entity.Person;

import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
//平台监管人员信息
public class PlatformSupervisor extends Person implements ObjectBean {
    @Override
    public void feedBack() {

    }

}
