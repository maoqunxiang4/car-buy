package com.xiaomaotongzhi.randomBuy.entity.Person;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Adapter.PersonAdapter;
import com.xiaomaotongzhi.randomBuy.entity.FeedBack.Feedback;
import com.xiaomaotongzhi.randomBuy.utils.MapperFactory;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

public abstract class Person {
    protected String id ;
    protected String mobile ;
    protected String username ;
    protected String email ;
    protected String address ;
    @TableField(value = "register_date")
    protected LocalDateTime register_date ;
    @TableField(value = "last_login")
    protected LocalDateTime last_login ;
    protected int is_delete ;
    @TableField(exist = false)
    protected Feedback feedback ;

    public abstract void feedBack() ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDateTime getRegister_date() {
        return register_date;
    }

    public void setRegister_date(LocalDateTime register_date) {
        this.register_date = register_date;
    }

    public LocalDateTime getLast_login() {
        return last_login;
    }

    public void setLast_login(LocalDateTime last_login) {
        this.last_login = last_login;
    }

    public int getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(int is_delete) {
        this.is_delete = is_delete;
    }
}
