package com.xiaomaotongzhi.randomBuy.entity.Person;

public class DriverFactory implements PersonFactory {

    @Override
    public Person createPerson() {
        return new Driver();
    }
}
