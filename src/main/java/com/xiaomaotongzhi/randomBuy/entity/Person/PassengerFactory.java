package com.xiaomaotongzhi.randomBuy.entity.Person;

public class PassengerFactory implements PersonFactory {
    @Override
    public Person createPerson() {
        return new Passenger();
    }
}
