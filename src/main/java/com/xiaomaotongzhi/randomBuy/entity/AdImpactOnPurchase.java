package com.xiaomaotongzhi.randomBuy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdImpactOnPurchase {
    private int impactId;              // 记录ID
    private int adId;                  // 广告ID
    private int userId;                // 用户ID
    private Date adViewDate;           // 用户观看广告的日期和时间
    private boolean ledToPurchase;     // 广告是否导致用户购买
    private int productId;             // 如果用户购买了，购买的商品ID
    private Date purchaseDate;         // 购买日期和时间
}
