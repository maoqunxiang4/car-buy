package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

public class DriverFeedbackFactory implements FeedbackFactory {

    @Override
    public Feedback createFeedback() {
        return new DriverFeedback() ;
    }
}
