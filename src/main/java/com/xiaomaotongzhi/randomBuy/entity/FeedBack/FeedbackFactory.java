package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

public interface FeedbackFactory {
    Feedback createFeedback() ;
}
