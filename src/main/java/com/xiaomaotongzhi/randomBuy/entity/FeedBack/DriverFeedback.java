package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

@Data
public class DriverFeedback extends Feedback implements ObjectBean {
    private String driverId;              // 司机ID
}
