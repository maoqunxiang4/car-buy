package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;

public class AdFeedbackFactory implements FeedbackFactory {
    @Override
    public Feedback createFeedback() {
        return new AdFeedback();
    }
}
