package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

public class FeedbackStore {
    private static final AdFeedbackFactory adFeedbackFactory = new AdFeedbackFactory();
    private static final PassengerFeedbackFactory userFeedbackFactory = new PassengerFeedbackFactory();
    private static final DriverFeedbackFactory driverFeedbackFactory = new DriverFeedbackFactory();

    private FeedbackStore() {
    }

    private static final class FeedbackHolder {
        private static final FeedbackStore feedbackStore = new FeedbackStore();
    }

    public FeedbackStore getInstance() {
        return FeedbackHolder.feedbackStore ;
    }

    public Feedback getFeedback(Class<? extends Feedback> clazz) {
        if (clazz.equals(AdFeedback.class)) {
            return adFeedbackFactory.createFeedback();
        } else if (clazz.equals(DriverFeedback.class)) {
            return driverFeedbackFactory.createFeedback();
        } else if (clazz.equals(PassengerFeedback.class)) {
            return userFeedbackFactory.createFeedback();
        }
        return null ;
    }
}
