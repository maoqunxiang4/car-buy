package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

public class PassengerFeedbackFactory implements FeedbackFactory{
    @Override
    public Feedback createFeedback() {
        return new PassengerFeedback();
    }
}
