package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

import lombok.Data;

@Data
public abstract class Feedback {
    protected String id ;
    protected String userId ;
    protected String feedbackType ;
    protected String feedbackDate ;
    protected int rating;                // 评分（例如：1到5星）
}
