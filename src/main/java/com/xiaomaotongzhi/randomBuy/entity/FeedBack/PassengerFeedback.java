package com.xiaomaotongzhi.randomBuy.entity.FeedBack;

import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

@Data
//反馈
public class PassengerFeedback extends Feedback implements ObjectBean {
    private String productId ;
}
