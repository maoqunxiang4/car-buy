package com.xiaomaotongzhi.randomBuy.entity.Adapter;

import com.xiaomaotongzhi.randomBuy.mapper.StationStaffMapper;
import com.xiaomaotongzhi.randomBuy.entity.Person.StationStaff;
import com.xiaomaotongzhi.randomBuy.utils.UsualUtils;

import java.time.LocalDateTime;

public class StationStaffCreateUser extends PersonAdapter {
    @Override
    public void createUser(String id, String stationId, String role) {
        super.createUser(id, stationId, role);
        LocalDateTime now = UsualUtils.getTimeNow();
        StationStaff stationStaff = new StationStaff();
        stationStaff.setId(id);
        stationStaff.setIs_delete(0);
        stationStaff.setLast_login(now) ;
        stationStaff.setRegister_date(now);
        stationStaff.setStationId(stationId);
        stationStaff.setRole(role);
        StationStaffMapper stationStaffMapper = (StationStaffMapper) mapperFactory.getToolHandler("stationStaffMapper");
        stationStaffMapper.insert(stationStaff) ;
    }
}
