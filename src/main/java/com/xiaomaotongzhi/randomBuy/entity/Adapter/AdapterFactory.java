package com.xiaomaotongzhi.randomBuy.entity.Adapter;

import com.xiaomaotongzhi.randomBuy.entity.Person.*;

public class AdapterFactory {

    private static PersonAdapter personAdapter ;

    private AdapterFactory() {
    }

    public static PersonAdapter getPersonAdapter(Class<? extends Person> clazz) {
        if (clazz.equals(Passenger.class)) {
            personAdapter = new PassengerCreateUser() ;
        } else if (clazz.equals(Driver.class)) {
            personAdapter = new DriverCreateUser() ;
        } else if (clazz.equals(StationStaff.class)) {
            personAdapter = new StationStaffCreateUser() ;
        } else if (clazz.equals(PlatformSupervisor.class)) {
            personAdapter = new PlatformSupervisorCreateUser() ;
        }
        return personAdapter ;
    }
}
