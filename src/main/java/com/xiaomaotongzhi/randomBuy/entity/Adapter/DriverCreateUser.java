package com.xiaomaotongzhi.randomBuy.entity.Adapter;

import com.xiaomaotongzhi.randomBuy.mapper.DriverMapper;
import com.xiaomaotongzhi.randomBuy.entity.Person.Driver;
import com.xiaomaotongzhi.randomBuy.utils.UsualUtils;

import java.time.LocalDateTime;

public class DriverCreateUser extends PersonAdapter {
    @Override
    public void createUser(String id, String vehicleId) {
        super.createUser(id, vehicleId);
        LocalDateTime now = UsualUtils.getTimeNow();
        Driver driver = new Driver();
        driver.setId(id);
        driver.setIs_delete(0);
        driver.setLast_login(now) ;
        driver.setRegister_date(now);
        driver.setVehicleId(vehicleId);
        DriverMapper driverMapper = (DriverMapper) mapperFactory.getToolHandler("driverMapper");
        driverMapper.insert(driver) ;
    }
}
