package com.xiaomaotongzhi.randomBuy.entity.Adapter;

public interface PersonCreateUser {
    void createUser(String id) ;
    void createUser(String id ,String stationId ,String role) ;
    void createUser(String id ,String vehicleId) ;
}
