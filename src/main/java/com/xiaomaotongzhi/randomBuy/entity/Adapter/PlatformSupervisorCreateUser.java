package com.xiaomaotongzhi.randomBuy.entity.Adapter;

import com.xiaomaotongzhi.randomBuy.mapper.PlatformSupervisorMapper;
import com.xiaomaotongzhi.randomBuy.entity.Person.PlatformSupervisor;
import com.xiaomaotongzhi.randomBuy.utils.UsualUtils;

import java.time.LocalDateTime;

public class PlatformSupervisorCreateUser extends PersonAdapter{
    @Override
    public void createUser(String id) {
        super.createUser(id);
        LocalDateTime now = UsualUtils.getTimeNow();
        PlatformSupervisor platformSupervisor = new PlatformSupervisor();
        platformSupervisor.setId(id);
        platformSupervisor.setIs_delete(0);
        platformSupervisor.setLast_login(now) ;
        platformSupervisor.setRegister_date(now);
        PlatformSupervisorMapper platformSupervisorMapper = (PlatformSupervisorMapper) mapperFactory.getToolHandler("platformSupervisorMapper");
        platformSupervisorMapper.insert(platformSupervisor) ;
    }
}
