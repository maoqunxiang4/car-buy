package com.xiaomaotongzhi.randomBuy.entity.Adapter;

import com.xiaomaotongzhi.randomBuy.entity.Person.PersonGathering;
import com.xiaomaotongzhi.randomBuy.utils.MapperFactory;
import org.springframework.stereotype.Component;

@Component
public abstract class PersonAdapter implements PersonCreateUser {
    protected MapperFactory mapperFactory = MapperFactory.getInstance() ;

    protected PersonGathering personGathering = PersonGathering.getInstance() ;

//    public PersonAdapter() {
//
//    }
//
//    @Autowired
//    public PersonAdapter(MapperFactory mapperFactory) {
//        this.mapperFactory = mapperFactory ;
//    }

    @Override
    public void createUser(String id) {

    }

    @Override
    public void createUser(String id, String stationId, String role) {

    }

    @Override
    public void createUser(String id, String vehicleId) {

    }
}
