package com.xiaomaotongzhi.randomBuy.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("user_coupon")
@Builder
public class UserCoupon {

    @TableId(value = "id")
    private String id;

    @TableField("user_id")
    private String userId;

    @TableField("coupon_id")
    private String couponId;

    @TableField("status")
    private String status;  // 如：CLAIMED, USED

    @TableField("claimed_at")
    private LocalDateTime claimedAt;  // 领取时间

    @TableField("used_at")
    private LocalDateTime usedAt;  // 使用时间
}

