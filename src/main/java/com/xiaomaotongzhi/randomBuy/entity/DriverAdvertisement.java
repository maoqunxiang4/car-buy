package com.xiaomaotongzhi.randomBuy.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DriverAdvertisement {
    private String driverId ;
    private String advertisementId ;
}
