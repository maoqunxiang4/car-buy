package com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord;

import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AdTotalPlaybackRecord extends TotalRecord implements ObjectBean {
}
