package com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;
import lombok.Data;

@Data
public abstract class TotalRecord extends Record {
    protected String additionId ;
    protected double totalNumber ;
}
