package com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;
import lombok.Data;

@Data
public abstract class LocationRecord extends Record {
    protected double x ;
    protected double y ;
}
