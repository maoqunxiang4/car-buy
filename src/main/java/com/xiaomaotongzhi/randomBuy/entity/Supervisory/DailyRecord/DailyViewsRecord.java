package com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyRecord;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailyViewsRecord extends DailyRecord implements ObjectBean {
    @TableField("ad_id")
    private String adId;                  // 广告ID
    @TableField("daily_ad_views")
    private double dailyAdViews;          // 广告单日播放量
}
