package com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
//销售记录
public class SalesRecord extends LocationRecord implements ObjectBean {
    @TableField("driver_id")
    private String driverId ;
    @TableField("passenger_id")
    private String passengerId ;
    @TableField("sale_date")
    private LocalDateTime saleDate ;
    @TableField("sale_price")
    private double salePrice ;
}
