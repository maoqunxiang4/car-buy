package com.xiaomaotongzhi.randomBuy.entity.Supervisory.RecordFactory;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.AdTotalPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.ProductTotalSalesRecord;

public class TotalRecordFactory implements RecordFactory {
    @Override
    public Record createRecord(Class<? extends Record> clazz) {
        if (clazz.equals(AdTotalPlaybackRecord.class)) {
            return new AdTotalPlaybackRecord();
        } else if (clazz.equals(ProductTotalSalesRecord.class)) {
            return new ProductTotalSalesRecord();
        }
        return null;
    }
}
