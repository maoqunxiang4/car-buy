package com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//记录司机广告的播放情况
public class DailyDriverAdPlaybackRecord extends DailyRecord implements ObjectBean {
    @TableField("ad_id")
    private String adId ;
    @TableField("driver_id")
    private String driverId ;
    @TableField("number_of_views")
    private double numberOfViews ;
}
