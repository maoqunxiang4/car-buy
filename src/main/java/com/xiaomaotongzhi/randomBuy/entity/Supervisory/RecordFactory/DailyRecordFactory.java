package com.xiaomaotongzhi.randomBuy.entity.Supervisory.RecordFactory;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyDriverAdPlaybackRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailySalesRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyViewsRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;

public class DailyRecordFactory implements RecordFactory {

    @Override
    public Record createRecord(Class<? extends Record> clazz) {
        if (clazz.equals(DailyViewsRecord.class)) {
            return new DailyViewsRecord() ;
        } else if (clazz.equals(DailySalesRecord.class)) {
            return new DailySalesRecord() ;
        } else if (clazz.equals(DailyDriverAdPlaybackRecord.class)) {
            return new DailyDriverAdPlaybackRecord() ;
        }
        return null;
    }
}
