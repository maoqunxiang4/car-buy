package com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyRecord;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
//日销售记录
public class DailySalesRecord extends DailyRecord implements ObjectBean {
    @TableField("product_id")
    private String productId;             // 商品ID
    @TableField("daily_sales_amount")
    private double dailySalesAmount;   // 商品单日销售额
}
