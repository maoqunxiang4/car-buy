package com.xiaomaotongzhi.randomBuy.entity.Supervisory.RecordFactory;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;

public interface RecordFactory {
    Record createRecord(Class<? extends Record> clazz) ;
}
