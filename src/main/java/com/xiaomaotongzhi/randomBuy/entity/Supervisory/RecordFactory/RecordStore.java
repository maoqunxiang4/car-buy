package com.xiaomaotongzhi.randomBuy.entity.Supervisory.RecordFactory;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord.DailyRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.LocationRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord.TotalRecord;

public class RecordStore {
    private final static DailyRecordFactory dailyRecordFactory = new DailyRecordFactory() ;
    private final static LocationRecordFactory locationRecordFactory = new LocationRecordFactory();
    private final static TotalRecordFactory totalRecordFactory = new TotalRecordFactory() ;

    private RecordStore() {
    }

    private static final class RecordStoreHolder {
        private static final RecordStore recordStore = new RecordStore();
    }

    public static RecordStore getInstance() {
        return RecordStoreHolder.recordStore ;
    }

    public Record getRecord(Class<? extends Record> clazz) {
        if (DailyRecord.class.isAssignableFrom(clazz)) {
            return dailyRecordFactory.createRecord(clazz);
        } else if (LocationRecord.class.isAssignableFrom(clazz)) {
            return locationRecordFactory.createRecord(clazz) ;
        } else if (TotalRecord.class.isAssignableFrom(clazz)) {
            return totalRecordFactory.createRecord(clazz) ;
        }
        return null ;
    }

}
