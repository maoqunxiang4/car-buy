package com.xiaomaotongzhi.randomBuy.entity.Supervisory.RecordFactory;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.SalesRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.UserBehaviorRecord;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;

public class LocationRecordFactory implements RecordFactory{

    @Override
    public Record createRecord(Class<? extends Record> clazz) {
        if (clazz.equals(SalesRecord.class)) {
            return new SalesRecord() ;
        } else if (clazz.equals(UserBehaviorRecord.class)) {
            return new UserBehaviorRecord() ;
        }
        return null;
    }
}
