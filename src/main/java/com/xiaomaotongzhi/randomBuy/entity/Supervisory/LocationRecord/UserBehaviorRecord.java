package com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.LocationRecord.LocationRecord;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
//用户行为记录
public class UserBehaviorRecord extends LocationRecord implements ObjectBean {
    @TableField("user_id")
    private String userId ;
    @TableField("product_id")
    private String productId ;
    @TableField("behavior_type")
    private String behaviorType ;
    @TableField("behavior_date")
    private LocalDateTime behaviorDate ;
}
