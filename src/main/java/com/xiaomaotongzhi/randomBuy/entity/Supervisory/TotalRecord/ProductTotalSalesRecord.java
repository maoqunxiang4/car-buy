package com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductTotalSalesRecord extends TotalRecord implements ObjectBean {
    @TableField("total_revenue")
    private double totalRevenue;          // 总销售额
    @TableField("average_price")
    private double averagePrice;          // 平均销售价格
    @TableField("img_url")
    private String imgUrl ;
}
