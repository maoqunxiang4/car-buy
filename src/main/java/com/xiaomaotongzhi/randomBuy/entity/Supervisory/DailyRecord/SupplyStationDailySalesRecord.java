package com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

@Data
@TableName("supply_station_daily_sales_record")
public class SupplyStationDailySalesRecord extends DailyRecord implements ObjectBean {
    @TableField("product_id")
    private String productId ; // Key: 商品ID, Value: 当日销售数量
    @TableField("product_value")
    private double productValue ;
    @TableField("daily_revenue")
    private double dailyRevenue; // 当日收入
}
