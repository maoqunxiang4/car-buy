package com.xiaomaotongzhi.randomBuy.entity.Supervisory.TotalRecord;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

@Data
public class SupplyStationTotalSalesRecord extends TotalRecord implements ObjectBean {
    @TableField("station_id")
    private String stationId;
    @TableField("total_revenue")
    private double totalRevenue; // 总收入
}
