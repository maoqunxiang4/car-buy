package com.xiaomaotongzhi.randomBuy.entity.Supervisory.DailyRecord;

import com.xiaomaotongzhi.randomBuy.entity.Supervisory.Record;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public abstract class DailyRecord extends Record {
    protected LocalDateTime date;                 // 日期
}
