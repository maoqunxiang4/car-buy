package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;

public abstract class AdditionBuilder {
    protected Addition addition ;

    public void setAddition(Addition addition) {
        this.addition = addition;
    }

    public abstract AdditionBuilder injectAscription () ;
    public abstract AdditionBuilder injectInvariant () ;
    public abstract AdditionBuilder injectChange () ;
    public abstract Addition build () ;
}
