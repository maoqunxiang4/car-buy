package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Product;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class ProductBuilder extends AdditionBuilder{
    Product product = (Product) addition ;
    private String id;                   // 产品ID
    private String productName;          // 产品名称
    private String description;          // 产品描述
    private Double price;                // 产品价格
    private String imageUrl;             // 产品图片URL
    private String category;             // 产品类别
    private LocalDateTime createdAt;         // 创建时间
    private LocalDateTime updatedAt;         // 更新时间

    @Override
    public AdditionBuilder injectAscription() {
        product.setId(id);
        return this;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        product.setProductName(productName);
        product.setDescription(description);
        return this;
    }

    @Override
    public AdditionBuilder injectChange() {
        product.setPrice(price);
        product.setImageUrl(imageUrl);
        product.setCategory(category);
        product.setCreatedAt(createdAt);
        product.setUpdatedAt(updatedAt);
        return this;
    }

    @Override
    public Addition build() {
        return product;
    }
}
