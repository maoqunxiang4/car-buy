package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.AdvertisementStrategy;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.SupplyStationStrategy;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;

//补货站
@Builder
@Data
public class SupplyStation extends Addition implements ObjectBean {
    private String stationName ;
    private String contact ;
    private String operatingHours ; //    营业时间
    @TableField("location")
    private String location ;
    //地点
    private BigDecimal x ;
    private BigDecimal y ;

    @Override
    public void useStrategy(HashMap<String,Object> objectHashMap) {
        if (strategy instanceof SupplyStationStrategy){
            SupplyStationStrategy supplyStationStrategy = (SupplyStationStrategy) strategy;
            supplyStationStrategy.afterDoing();
        }
    }
}
