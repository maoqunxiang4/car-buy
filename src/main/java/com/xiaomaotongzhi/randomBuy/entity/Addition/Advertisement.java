package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.AdvertisementStrategy;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;

@Data
//广告
public class Advertisement extends Addition implements ObjectBean {
    private String title ;
    private String content ;
    private String vertiser ;//广告商
    @TableField("start_date")
    private LocalDateTime startDate ;
    @TableField("end_date")
    private LocalDateTime endDate ;
    @TableField("target_audience")
    private String targetAudience ;

    @Override
    public void useStrategy(HashMap<String,Object> objectHashMap) {
        if (strategy instanceof AdvertisementStrategy){
            AdvertisementStrategy advertisementStrategy = (AdvertisementStrategy) strategy;
            advertisementStrategy.afterDoing();
        }
    }
}
