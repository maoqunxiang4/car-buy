package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaomaotongzhi.randomBuy.utils.StatusEnum;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@TableName("coupon")
public class Coupon extends Addition {
    @TableField("title")
    private String title;  // 优惠券标题

    @TableField("description")
    private String description;  // 优惠券描述

    @TableField("discount_amount")
    private Double discountAmount;  // 折扣金额

    @TableField("minimum_purchase")
    private Double minimumPurchase;  // 最低消费金额

    @TableField("start_date")
    private LocalDateTime startDate;  // 有效开始日期

    @TableField("end_date")
    private LocalDateTime endDate;  // 有效结束日期

    @TableField("status")
    private StatusEnum status;  // 优惠券状态

    @TableField("created_at")
    private LocalDateTime createdAt;  // 创建时间

    @TableField("updated_at")
    private LocalDateTime updatedAt;  // 更新时间

    @Override
    public void useStrategy(HashMap<String, Object> objectHashMap) {

    }
}

