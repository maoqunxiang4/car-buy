package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.xiaomaotongzhi.randomBuy.entity.Mediator.AdvertisementStrategy;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;

//设备
@Data
public class Device extends Addition implements ObjectBean {
    private String driverId ;
    private String status ;
    private LocalDateTime lastCheck ;
    private String model ;

    @Override
    public void useStrategy(HashMap<String,Object> objectHashMap) {
    }
}
