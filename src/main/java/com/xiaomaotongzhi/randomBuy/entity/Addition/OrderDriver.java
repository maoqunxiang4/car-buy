package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@TableName("order_driver")
public class OrderDriver extends Addition implements ObjectBean {
    private String productId ;
    private String passengerId ;
    private String driverId ;
    private LocalDateTime orderDate ;
    private double amount ;
    private String vehicleId ;
    private Double price ;

    @Override
    public void useStrategy(HashMap<String,Object> objectHashMap) {

    }
}
