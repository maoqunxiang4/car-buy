package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.ProductStrategy;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.Data;

import java.util.HashMap;

@Data
@TableName("driver_product")
public class DriverProduct extends Addition implements ObjectBean {
    private String productName ;
    private String description ;
    private Double price ;
    private Integer stock ;
    private String imageUrl ;
    private String category ;
    private String driver_id ;

    @Override
    public void useStrategy(HashMap<String,Object> objectHashMap) {
        if (strategy instanceof ProductStrategy){
            ProductStrategy productStrategy = (ProductStrategy) strategy;
            productStrategy.afterDoing();
        }
    }
}
