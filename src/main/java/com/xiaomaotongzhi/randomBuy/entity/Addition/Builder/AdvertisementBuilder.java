package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Advertisement;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Builder
@Data
public class AdvertisementBuilder extends AdditionBuilder {

    private final Advertisement advertisement = (Advertisement) addition ;
    private String id ;
    private String title ;
    private String content ;
    private String vertiser ;//广告商
    private LocalDateTime startDate ;
    private LocalDateTime endDate ;
    private String targetAudience ;

    @Override
    public AdditionBuilder injectAscription() {
        advertisement.setId(id);
        return this;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        advertisement.setTitle(title);
        advertisement.setContent(content);
        advertisement.setVertiser(vertiser);
        advertisement.setTargetAudience(targetAudience);
        return this;
    }

    @Override
    public AdditionBuilder injectChange() {
        advertisement.setStartDate(startDate);
        advertisement.setEndDate(endDate);
        return this;
    }

    @Override
    public Addition build() {
        return advertisement;
    }
}
