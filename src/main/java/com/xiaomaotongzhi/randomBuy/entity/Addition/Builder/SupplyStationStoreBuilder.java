package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.SupplyStationStore;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class SupplyStationStoreBuilder extends AdditionBuilder{

    SupplyStationStore supplyStationStore = (SupplyStationStore) addition ;
    private String id ;
    private String supplyStationId;      // 补货站ID
    private String productId;            // 产品ID
    private Long stockQuantity;       // 存货数量
    private LocalDateTime lastUpdated;       // 最后更新时间

    @Override
    public AdditionBuilder injectAscription() {
        supplyStationStore.setId(id);
        supplyStationStore.setSupplyStationId(supplyStationId);
        return this;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        supplyStationStore.setProductId(productId);
        supplyStationStore.setProductId(id) ;
        return this;
    }

    @Override
    public AdditionBuilder injectChange() {
        supplyStationStore.setStockQuantity(stockQuantity);
        supplyStationStore.setLastUpdated(lastUpdated);
        return this;
    }

    @Override
    public Addition build() {
        return supplyStationStore;
    }
}
