package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Device;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class DeviceBuilder extends AdditionBuilder {

    private final Device device = (Device) addition ;
    private String id ;
    private String driverId ;
    private String status ;
    private LocalDateTime lastCheck ;
    private String model ;



    @Override
    public AdditionBuilder injectAscription() {
        device.setId(id);
        device.setDriverId(driverId);
        return this ;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        device.setModel(model);
        return this ;
    }

    @Override
    public AdditionBuilder injectChange() {
        device.setStatus(status);
        device.setLastCheck(lastCheck);
        return this ;
    }

    @Override
    public Addition build() {
        return device;
    }


}
