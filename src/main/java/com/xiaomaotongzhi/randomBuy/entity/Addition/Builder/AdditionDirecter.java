package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.*;
import com.xiaomaotongzhi.randomBuy.utils.SnowflakeIdGenerator;

import java.time.LocalDateTime;

public class AdditionDirecter {

    private final SnowflakeIdGenerator snowflakeIdGenerator = new SnowflakeIdGenerator() ;

    private AdditionDirecter() {
    }

    private static final class AdditionDirecterHolder {
        private static final AdditionDirecter additionDirecter = new AdditionDirecter() ;
    }

    public static AdditionDirecter getInstance() {
        return AdditionDirecterHolder.additionDirecter;
    }

    public Device buildDevice(String id, String driver_id , String status , LocalDateTime last_check , String model ) {
        DeviceBuilder build = DeviceBuilder.builder()
                .id(id)
                .driverId(driver_id)
                .status(status)
                .lastCheck(last_check)
                .model(model).build();
        return (Device) build.injectAscription().injectInvariant().injectChange().build() ;
    }

    public Advertisement buildAdvertisement(String id , String title , String content , String vertiser , LocalDateTime startDate ,
                                            LocalDateTime endDate , String targetAudience ) {
        AdvertisementBuilder advertisementBuilder = AdvertisementBuilder.builder()
                .id(id)
                .title(title)
                .content(content)
                .vertiser(vertiser)
                .startDate(startDate)
                .endDate(endDate)
                .targetAudience(targetAudience).build();
        return (Advertisement) advertisementBuilder.injectAscription().injectInvariant().injectChange().build() ;
    }

    public OrderDriver buildOrder(String product_id , String passenger_id , String driver_id , LocalDateTime order_date ,
                                  double amount , String vehicle_id ,double price) {
        OrderBuilder orderBuilder = OrderBuilder.builder()
                .id(String.valueOf(snowflakeIdGenerator.nextId()))
                .productId(product_id)
                .passengerId(passenger_id)
                .driverId(driver_id)
                .orderDate(order_date)
                .amount(amount)
                .vehicleID(vehicle_id)
                .price(price)
                .build();
        return (OrderDriver) orderBuilder.injectAscription().injectInvariant().injectChange().build() ;
    }

    public DriverProduct buildPorduct(String id , String product_name , String description , Double price , Integer stock ,
                                      String image_url , String category , String driver_id ) {
        DriverProductBuilder driverProductBuilder = DriverProductBuilder.builder()
                .id(id)
                .product_name(product_name)
                .description(description)
                .price(price)
                .stock(stock)
                .image_url(image_url)
                .category(category)
                .driver_id(driver_id).build();
        return (DriverProduct) driverProductBuilder.injectAscription().injectInvariant().injectChange().build();
    }

    public Product buildSupplyStationProduct(String id, String productName , String description , Double price
            , String imageUrl , String category , LocalDateTime createdAt , LocalDateTime updatedAt) {
        ProductBuilder productBuilder = ProductBuilder.builder()
                .id(id)
                .productName(productName)
                .description(description)
                .price(price)
                .imageUrl(imageUrl)
                .category(category)
                .createdAt(createdAt)
                .updatedAt(updatedAt).build();
        return (Product) productBuilder.injectAscription().injectInvariant().injectChange().build();
    }


    public SupplyStationStore buildSupplyStationStore(String id , String supplyStationId ,
     String productId , Long stockQuantity , LocalDateTime lastUpdated        ) {
        SupplyStationStoreBuilder supplyStationStoreBuilder = SupplyStationStoreBuilder.builder()
                .id(id)
                .supplyStationId(supplyStationId)
                .productId(productId)
                .stockQuantity(stockQuantity)
                .lastUpdated(lastUpdated).build();
        return (SupplyStationStore) supplyStationStoreBuilder.injectAscription().injectInvariant().injectChange().build();
    }
}
