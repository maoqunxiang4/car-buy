package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Coupon;
import com.xiaomaotongzhi.randomBuy.utils.StatusEnum;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class CouponBuilder extends AdditionBuilder {

    Coupon coupon = (Coupon) addition ;
    private String id;
    private String title;  // 优惠券标题
    private String description;  // 优惠券描述
    private Double discountAmount;  // 折扣金额
    private Double minimumPurchase;  // 最低消费金额
    private LocalDateTime startDate;  // 有效开始日期
    private LocalDateTime endDate;  // 有效结束日期
    private StatusEnum status;  // 优惠券状态
    private LocalDateTime createdAt;  // 创建时间
    private LocalDateTime updatedAt;  // 更新时间

    @Override
    public AdditionBuilder injectAscription() {
        coupon.setId(id);
        return this;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        coupon.setTitle(title);
        coupon.setDescription(description);
        coupon.setDiscountAmount(discountAmount);
        coupon.setMinimumPurchase(minimumPurchase);
        coupon.setStartDate(startDate);
        coupon.setEndDate(endDate);
        coupon.setCreatedAt(createdAt);
        return this;
    }

    @Override
    public AdditionBuilder injectChange() {
        coupon.setStatus(status);
        coupon.setUpdatedAt(updatedAt);
        return this;
    }

    @Override
    public Addition build() {
        return coupon;
    }
}
