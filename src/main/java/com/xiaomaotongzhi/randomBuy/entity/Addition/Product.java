package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("product")
public class Product extends Addition{
    @TableField("product_name")
    private String productName;          // 产品名称
    @TableField("description")
    private String description;          // 产品描述
    @TableField("price")
    private Double price;                // 产品价格
    @TableField("image_url")
    private String imageUrl;             // 产品图片URL
    @TableField("category")
    private String category;             // 产品类别
    @TableField("created_at")
    private LocalDateTime createdAt;         // 创建时间
    @TableField("updated_at")
    private LocalDateTime updatedAt;         // 更新时间

    @Override
    public void useStrategy(HashMap<String, Object> objectHashMap) {

    }
}
