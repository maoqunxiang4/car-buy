package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.xiaomaotongzhi.randomBuy.utils.ObjectBean;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("supply_station_order")
public class SupplyStationOrder extends Addition implements ObjectBean {
    private String productId;          // 产品ID
    private Integer productNumber;   // 订单日期
    private LocalDateTime orderDate;   // 订单日期
    private double amount;             // 订单金额
    @TableField("supply_station_id")
    private String supplyStationId;    // 补货站ID

    @Override
    public void useStrategy(HashMap<String, Object> objectHashMap) {

    }
}
