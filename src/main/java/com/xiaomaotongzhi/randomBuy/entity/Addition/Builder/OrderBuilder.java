package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.OrderDriver;
import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public class OrderBuilder extends AdditionBuilder{

    private final OrderDriver order = new OrderDriver() ;
    private String id ;
    private String productId ;
    private String passengerId ;
    private String driverId ;
    private LocalDateTime orderDate ;
    private double amount ;
    private String vehicleID ;
    private double price ;

    @Override
    public AdditionBuilder injectAscription() {
        order.setId(id);
        order.setProductId(productId);
        order.setPassengerId(passengerId);
        order.setDriverId(driverId);
        return this ;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        order.setVehicleId(vehicleID);
        return this ;
    }

    @Override
    public AdditionBuilder injectChange() {
        order.setOrderDate(orderDate);
        order.setAmount(amount);
        order.setPrice(price);
        return this ;
    }

    @Override
    public Addition build() {
        return order;
    }


}
