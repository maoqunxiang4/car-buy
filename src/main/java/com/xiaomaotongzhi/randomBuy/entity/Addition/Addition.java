package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.xiaomaotongzhi.randomBuy.entity.Mediator.Strategy;

import java.util.HashMap;

public abstract class Addition {
    protected String id ;
    @TableField(exist = false)
    protected Strategy strategy ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract void useStrategy(HashMap<String,Object> objectHashMap) ;
}
