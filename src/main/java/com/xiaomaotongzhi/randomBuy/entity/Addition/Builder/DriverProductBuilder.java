package com.xiaomaotongzhi.randomBuy.entity.Addition.Builder;

import com.xiaomaotongzhi.randomBuy.entity.Addition.Addition;
import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import lombok.Builder;

@Builder
public class DriverProductBuilder extends AdditionBuilder{

    private final DriverProduct driverProduct = (DriverProduct)addition;
    private String id ;
    private String product_name ;
    private String description ;
    private Double price ;
    private Integer stock ;
    private String image_url ;
    private String category ;
    private String driver_id ;


    @Override
    public AdditionBuilder injectAscription() {
        driverProduct.setId(id);
        driverProduct.setDriver_id(driver_id);
        return this ;
    }

    @Override
    public AdditionBuilder injectInvariant() {
        driverProduct.setProductName(product_name);
        driverProduct.setDescription(description);
        driverProduct.setImageUrl(image_url);
        driverProduct.setCategory(category);
        return this ;
    }

    @Override
    public AdditionBuilder injectChange() {
        driverProduct.setPrice(price);
        driverProduct.setStock(stock);
        return this ;
    }

    @Override
    public Addition build() {
        return driverProduct;
    }


}
