package com.xiaomaotongzhi.randomBuy.entity.Addition;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("supply_station_store")
public class SupplyStationStore extends Addition {
    @TableField("supply_station_id")
    private String supplyStationId;      // 补货站ID
    @TableField("product_id")
    private String productId;            // 产品ID
    @TableField("stock_quantity")
    private Long stockQuantity;       // 存货数量
    @TableField("last_updated")
    private LocalDateTime lastUpdated;       // 最后更新时间

    @Override
    public void useStrategy(HashMap<String, Object> objectHashMap) {

    }
}
