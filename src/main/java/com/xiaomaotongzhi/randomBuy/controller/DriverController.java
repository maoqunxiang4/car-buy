package com.xiaomaotongzhi.randomBuy.controller;

import com.xiaomaotongzhi.randomBuy.entity.Addition.*;
import com.xiaomaotongzhi.randomBuy.entity.Person.StationStaff;
import com.xiaomaotongzhi.randomBuy.service.IDriverService;
import com.xiaomaotongzhi.randomBuy.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/driver")
@Api(tags = "司机API")
public class DriverController {

    @Autowired
    private IDriverService driverService;

    @ApiOperation(value = "添加汽车信息", tags = "司机API - 车辆管理")
    @PostMapping("/driver/insertVehicle")
    public Result insertDriverVehicle(@RequestBody Device device) {
        return driverService.insertDevice(device);
    }

    @ApiOperation(value = "修改汽车信息", tags = "司机API - 车辆管理")
    @PostMapping("/driver/updateVehicle")
    public Result updateDriverVehicle(@RequestBody Device device) {
        return driverService.updateDriverVehicle(device);
    }

//    @ApiOperation(value = "提交补货申请", tags = "司机API - 补货管理")
//    @PostMapping("/driver/requestRestock")
//    public Result requestRestock(@RequestBody List<Product> products) {
//        return driverService.requestRestock(products);
//    }

    @ApiOperation(value = "查看广告播放统计", tags = "司机API - 广告统计")
    @GetMapping("/driver/adPlaybackStatistics")
    public Result getDriverAdPlaybackStatistics() {
        return driverService.getDriverAdPlaybackStatistics();
    }

    @ApiOperation(value = "查看当天营销数据", tags = "司机API - 销售数据")
    @GetMapping("/driver/dailySalesData")
    public Result getDriverDailySalesData() {
        return driverService.getDriverDailySalesData();
    }

//    @ApiOperation(value = "查找最近的供货站", tags = "供货站API - 位置查询")
//    @GetMapping("/supplyStation/findNearest")
//    public Result findNearestSupplyStations(@RequestParam double x, @RequestParam double y) {
//        return driverService.findNearestSupplyStations(x, y);
//    }

//    @ApiOperation(value = "响应供货站的补货邀约", tags = "司机API - 补货管理")
//    @PostMapping("/driver/respondToRestockInvitation")
//    public Result respondToRestockInvitation(@RequestParam String invitationId, @RequestParam boolean accept) {
//        return driverService.respondToRestockInvitation(invitationId, accept);
//    }

    @ApiOperation(value = "创建优惠券", tags = "优惠券API - 创建优惠券")
    @PostMapping("/insertCoupon")
    public Result insertCoupon(@RequestBody Coupon coupon) {
        return driverService.insertCoupon(coupon);
    }

    @ApiOperation(value = "更新优惠券", tags = "优惠券API - 更新优惠券")
    @PostMapping("/updateCoupon")
    public Result updateCoupon(@RequestBody Coupon coupon) {
        return driverService.updateCoupon(coupon);
    }

    @GetMapping("/listDriverProductsByCategory")
    @ApiOperation(value = "按照类别列举司机拥有的商品", notes = "提供商品类别和司机ID来列举商品", tags = "司机API - 按照类别列举商品")
    public Result listDriverProductsByCategory(
            @RequestParam String category,
            @RequestParam String driverId) {
        return driverService.ListDriverProductsByCategory(category, driverId) ;
    }

    @GetMapping("/listDriverAdditionCategory")
    @ApiOperation(value = "列举司机拥有的商品的所有类别", notes = "提供司机ID来列举商品类别", tags = "司机API - 列举商品类别")
    public Result listDriverAdditionCategory(
            @RequestParam String driverId) {
        return driverService.ListDriverAdditionCategory(driverId) ;
    }
}


