package com.xiaomaotongzhi.randomBuy.controller;

import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.entity.Addition.SupplyStation;
import com.xiaomaotongzhi.randomBuy.entity.Addition.SupplyStationStore;
import com.xiaomaotongzhi.randomBuy.entity.Person.StationStaff;
import com.xiaomaotongzhi.randomBuy.service.ISupplyStationService;
import com.xiaomaotongzhi.randomBuy.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/supplyStation")
@Api(tags = "补货站管理API")
public class SupplyStationController {

    @Autowired
    private ISupplyStationService supplyStationService;

    @ApiOperation(value = "获取取货地点", tags = "补货站管理API - 取货管理")
    @GetMapping("/pickupLocations/{supplyStationID}")
    public Result getPickupLocations(@PathVariable String supplyStationID) {
        return supplyStationService.getPickupLocations(supplyStationID);
    }

    @ApiOperation(value = "获取销售统计", tags = "补货站管理API - 销售统计")
    @GetMapping("/salesStatistics/{stationId}")
    public Result getSalesStatistics(@PathVariable String stationId) {
        return supplyStationService.getSalesStatistics(stationId);
    }

//    @ApiOperation(value = "查看补货申请", tags = "补货站管理API - 补货管理")
//    @GetMapping("/restockRequests")
//    public Result viewRestockRequests() {
//        return supplyStationService.viewRestockRequests();
//    }

//    @ApiOperation(value = "处理补货申请", tags = "补货站管理API - 补货管理")
//    @PostMapping("/processRestockRequest/{requestId}")
//    public Result processRestockRequest(@PathVariable String requestId, @RequestBody List<Product> products) {
//        return supplyStationService.processRestockRequest(requestId, products);
//    }

//    @ApiOperation(value = "向司机发送补货邀约", tags = "补货站管理API - 补货管理")
//    @PostMapping("/sendRestockInvitation/{driverId}")
//    public Result sendRestockInvitation(@PathVariable String driverId, @RequestBody List<Product> suggestedProducts) {
//        return supplyStationService.sendRestockInvitation(driverId, suggestedProducts);
//    }

    @DeleteMapping("/deleteProduct/{id}")
    @ApiOperation(value = "删除商品", tags = "补货站管理API - 商品管理")
    public Result deleteSupplyStationProduct(@ApiParam(value = "商品ID", required = true) @PathVariable String id) {
        return supplyStationService.deleteSupplyStationProduct(id);
    }

    @GetMapping("/getProductDetails")
    @ApiOperation(value = "获取商品详情", tags = "补货站管理API - 商品管理")
    public Result getSupplyStationProductDetails(
            @ApiParam(value = "商品ID", required = true) @RequestParam String productId,
            @ApiParam(value = "补货站ID", required = true) @RequestParam String stationId) {
        return supplyStationService.getSupplyStationProductDetails(productId, stationId);
    }

    @PostMapping("/addProduct")
    @ApiOperation(value = "添加商品", tags = "补货站管理API - 商品管理")
    public Result addSupplyStationProduct(
            @ApiParam(value = "商品ID", required = true) @RequestParam String productId,
            @ApiParam(value = "商品价格", required = true) @RequestParam double productPrice,
            @ApiParam(value = "库存数量", required = true) @RequestParam Integer stockQuantity) {
        return supplyStationService.addSupplyStationProduct(productId, productPrice, stockQuantity);
    }

    @PutMapping("/updateProductBatch")
    @ApiOperation(value = "批量修改商品库存", tags = "补货站管理API - 商品管理")
    public Result updateSupplyStationProductBatch(@ApiParam(value = "商品列表", required = true) @RequestBody List<DriverProduct> driverProductList) {
        return supplyStationService.updateSupplyStationProductBatch(driverProductList);
    }

    @GetMapping("/getTodayRevenue")
    @ApiOperation(value = "获取当天的收入记录", tags = "补货站管理API - 销售统计")
    public Result getTodayRevenue() {
        return supplyStationService.getTodayRevenue();
    }

    @GetMapping("/getWeeklyRevenue")
    @ApiOperation(value = "获取一周的收入记录", tags = "补货站管理API - 销售统计")
    public Result getWeeklyRevenue() {
        return supplyStationService.getWeeklyRevenue();
    }

    @GetMapping("/getMonthlyRevenue")
    @ApiOperation(value = "获取一月的收入记录", tags = "补货站管理API - 销售统计")
    public Result getMonthlyRevenue() {
        return supplyStationService.getMonthlyRevenue();
    }

    @GetMapping("/getYearlyRevenue")
    @ApiOperation(value = "获取一年的收入记录", tags = "补货站管理API - 销售统计")
    public Result getYearlyRevenue() {
        return supplyStationService.getYearlyRevenue();
    }

    @GetMapping("/getTotalRevenue")
    @ApiOperation(value = "获取总的收入记录", tags = "补货站管理API - 销售统计")
    public Result getTotalRevenue() {
        return supplyStationService.getTotalRevenue();
    }

    @ApiOperation(value = "查看供货站的员工列表", tags = "供货站API - 员工管理")
    @GetMapping("/supplyStation/staffList/{stationId}")
    public Result viewStaffList(@PathVariable String stationId) {
        return supplyStationService.viewStaffList(stationId);
    }

    @ApiOperation(value = "添加供货站员工信息", tags = "供货站API - 员工管理")
    @PostMapping("/supplyStation/insertStaff")
    public Result insertStaff(@RequestBody StationStaff stationStaff) {
        return supplyStationService.insertStaff(stationStaff);
    }

    @ApiOperation(value = "更新供货站员工信息", tags = "供货站API - 员工管理")
    @PostMapping("/supplyStation/updateStaff")
    public Result updateStaff(@RequestBody StationStaff stationStaff) {
        return supplyStationService.updateStaff(stationStaff);
    }

    @ApiOperation(value = "修改供货站人员的角色", tags = "供货站API - 员工管理 ")
    @PutMapping("/supplyStation/updateRole/{stationStaffId}")
    public Result updateStationStaffRole(@PathVariable String stationStaffId , @RequestParam String role) {
        return supplyStationService.updateStationStaffRole(stationStaffId , role);
    }

    @ApiOperation(value = "查看供货站信息", tags = "供货站API - 信息查看")
    @GetMapping("/supplyStation/profile/{stationId}")
    public Result getStationProfile(@PathVariable String stationId) {
        return supplyStationService.getStationProfile(stationId);
    }

    @ApiOperation(value = "更新供货站信息", tags = "供货站API - 信息更新")
    @PutMapping("/supplyStation/updateProfile")
    public Result updateStationProfile(@RequestBody SupplyStation supplyStation) {
        return supplyStationService.updateStationProfile(supplyStation);
    }

    @ApiOperation(value = "查看供货站库存信息", tags = "供货站API - 库存管理")
    @GetMapping("/supplyStation/inventory/{stationId}")
    public Result viewInventory(@PathVariable String stationId) {
        return supplyStationService.viewInventory(stationId);
    }

    @ApiOperation(value = "更新库存信息", tags = "供货站API - 库存管理")
    @PutMapping("/supplyStation/updateInventory/{stationId}")
    public Result updateInventory(@PathVariable String stationId, @RequestBody List<SupplyStationStore> supplyStationStoreList) {
        return supplyStationService.updateInventory(stationId, supplyStationStoreList);
    }

    @GetMapping("/viewSupplyStationInventoryByCategory")
    @ApiOperation(value = "按照类别查看补货站库存", notes = "提供补货站ID和商品类别来查看库存" , tags = "供货站API - 库存管理")
    public Result viewSupplyStationInventoryByCategory(
            @RequestParam String stationId,
            @RequestParam String category) {
        return supplyStationService.viewSupplyStationInventoryByCategory(stationId, category) ;
    }

    @GetMapping("/viewSupplyStationCategories")
    @ApiOperation(value = "查看补货站库存包含的类别", notes = "提供补货站ID查看补货站库存包含的类别" , tags = "供货站API - 库存管理")
    public Result viewSupplyStationCategories(
            @RequestParam String stationId) {
        return supplyStationService.viewSupplyStationCategories(stationId) ;
    }

}