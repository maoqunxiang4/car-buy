package com.xiaomaotongzhi.randomBuy.controller;

import com.xiaomaotongzhi.randomBuy.service.Impl.PassengerServiceImpl;
import com.xiaomaotongzhi.randomBuy.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Api(tags = "乘客API")
//OVER
public class PassengerController {
    @Autowired
    private PassengerServiceImpl passengerService;

    @GetMapping("/login")
    @ApiOperation(value = "用户登陆", tags = "乘客API - 用户登陆")
    public Result login(String code) {
        return passengerService.login(code);
    }

    @ApiOperation(value = "申请身份", tags = "乘客API - 乘客操作")
    @PostMapping("/passenger/verifyRole")
    public Result verifyRole(@RequestParam String verify, @RequestParam(required = false) String stationId, @RequestParam(required = false) String role, @RequestParam(required = false) String vehicleId) {
        return passengerService.verifyRole(verify, stationId, role, vehicleId);
    }

    @ApiOperation(value = "获取用户的优惠券信息", tags = "乘客API - 优惠券操作")
    @GetMapping("/passenger/coupons")
    public Result getUserCoupons() {
        return passengerService.getUserCoupons();
    }

    @ApiOperation(value = "用户领取优惠券", tags = "乘客API - 优惠券操作")
    @PostMapping("/passenger/claimCoupon/{couponId}")
    public Result claimCoupon(@PathVariable String couponId) {
        return passengerService.claimCoupon(couponId);
    }

    @ApiOperation(value = "使用优惠券", tags = "乘客API - 优惠券操作")
    @PostMapping("/passenger/useCoupon/{couponId}")
    public Result useCoupon(@PathVariable String couponId) {
        return passengerService.useCoupon(couponId);
    }

    @ApiOperation(value = "查看优惠券详情", tags = "乘客API - 优惠券操作")
    @GetMapping("/passenger/couponDetail/{couponId}")
    public Result getCouponDetail(@PathVariable String couponId) {
        return passengerService.getCouponDetail(couponId);
    }

    @ApiOperation(value = "获取所有的优惠券", tags = "乘客API - 优惠券操作")
    @GetMapping("/passenger/allCoupons")
    public Result getAllCoupons() {
        return passengerService.getAllCoupons();
    }

    @ApiOperation(value = "乘客查看个人信息", tags = "乘客API - 乘客操作")
    @GetMapping("/passenger/profile")
    public Result getUserProfile() {
        return passengerService.getUserProfile();
    }

    @ApiOperation(value = "乘客查看历史订单", tags = "乘客API - 订单操作")
    @GetMapping("/passenger/orderHistory/{userId}")
    public Result getUserOrderHistory() {
        return passengerService.getUserOrderHistory();
    }

    @ApiOperation(value = "乘客查看当前订单", tags = "乘客API - 订单操作")
    @GetMapping("/passenger/currentOrder/{userId}")
    public Result getUserCurrentOrder() {
        return passengerService.getUserCurrentOrder();
    }

    //TODO 用户缺少一个抢购优惠券的功能
}