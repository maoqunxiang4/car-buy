package com.xiaomaotongzhi.randomBuy.controller;

import com.xiaomaotongzhi.randomBuy.dto.BuyProductRequest;
import com.xiaomaotongzhi.randomBuy.entity.Addition.Advertisement;
import com.xiaomaotongzhi.randomBuy.entity.Addition.DriverProduct;
import com.xiaomaotongzhi.randomBuy.service.IAdditionService;
import com.xiaomaotongzhi.randomBuy.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/addition")
@Api(tags = "商品管理API")
public class AdditionController {

    @Autowired
    private IAdditionService additionService;

    // 产品管理
    @GetMapping("/products")
    @Operation(summary = "列出产品", description = "根据司机ID列出产品", tags = "商品管理API - 产品管理")
    public Result listProducts(
            @Parameter(description = "司机ID") @RequestParam String driverId,
            @Parameter(description = "页码") @RequestParam int page,
            @Parameter(description = "每页大小") @RequestParam int size) {
        return additionService.listProducts(driverId, page, size);
    }

    @GetMapping("/product/{productId}")
    @Operation(summary = "获取产品详情", description = "根据产品ID获取产品详情", tags = "商品管理API - 产品管理")
    public Result getProductDetails(@Parameter(description = "产品ID") @PathVariable String productId) {
        return additionService.getProductDetails(productId);
    }

    @PostMapping("/product")
    @Operation(summary = "添加产品", description = "添加一个新的产品", tags = "商品管理API - 产品管理")
    public Result addProduct(@RequestBody @Parameter(description = "产品信息") DriverProduct driverProductInfo) {
        return additionService.addProduct(driverProductInfo);
    }

    @PutMapping("/product")
    @Operation(summary = "更新产品", description = "更新现有的产品信息", tags = "商品管理API - 产品管理")
    public Result updateProduct(@RequestBody @Parameter(description = "产品信息") DriverProduct driverProductInfo) {
        return additionService.updateProduct(driverProductInfo);
    }

    @DeleteMapping("/product/{productId}")
    @Operation(summary = "删除产品", description = "根据产品ID删除产品", tags = "商品管理API - 产品管理")
    public Result deleteProduct(@Parameter(description = "产品ID") @PathVariable String productId) {
        return additionService.deleteProduct(productId);
    }

    @PostMapping("/buy")
    @Operation(summary = "购买商品", description = "购买商品", tags = "商品管理API - 订单管理")
    public Result buyProduct(
            @ApiParam(value = "购买请求详情", required = true) @RequestBody BuyProductRequest buyProductRequest,
            @ApiParam(value = "总价格", required = true) @RequestParam Double price,
            @ApiParam(value = "购买商品的数量", required = true) @RequestParam Integer amount,
            @ApiParam(value = "X坐标", required = true) @RequestParam String x,
            @ApiParam(value = "Y坐标", required = true) @RequestParam String y) {
        return additionService.buyProduct(buyProductRequest.getDriverProduct(), price , amount, buyProductRequest.getDriver(), x, y);
    }

    // 订单管理
    @GetMapping("/user/orders")
    @Operation(summary = "列出用户订单列表", description = "根据用户ID列出其所有订单", tags = "商品管理API - 订单管理")
    public Result listUserOrders(@Parameter(description = "用户ID") @RequestParam String userId) {
        return additionService.listUserOrders(userId);
    }

    @GetMapping("/order/{orderId}")
    @Operation(summary = "获取订单详情", description = "根据订单ID获取订单详情", tags = "商品管理API - 订单管理")
    public Result getOrderDetails(@Parameter(description = "订单ID") @PathVariable String orderId) {
        return additionService.getOrderDetails(orderId);
    }

//    // 广告管理
//    @GetMapping("/ad/playbacks")
//    @Operation(summary = "列出广告播放记录", description = "根据广告ID列出所有播放记录", tags = "商品管理API - 广告管理")
//    public Result listAdPlaybacks(@Parameter(description = "广告ID") @RequestParam String adId) {
//        return additionService.listAdPlaybacks(adId);
//    }

    @GetMapping("/ad/statistics/{driverId}")
    @Operation(summary = "获取广告播放统计", description = "根据广告ID获取播放统计信息", tags = "商品管理API - 广告管理")
    public Result getAdPlaybackStatistics(@Parameter(description = "司机ID") @PathVariable String driverId) {
        return additionService.getAdPlaybackStatistics(driverId);
    }

    @GetMapping("/listForDriver")
    @Operation(summary = "为司机列出广告", description = "driverID获取广告列表", tags = "商品管理API - 广告管理")
    public Result listAdsForDriver(
            @ApiParam(value = "司机ID", required = true) @RequestParam String driverId) {
        return additionService.listAdsForDriver(driverId);
    }

    @PostMapping("/watch")
    @Operation(summary = "观看广告", description = "通过广告ID和司机ID，在广告播放后的进行相应的处理", tags = "商品管理API - 广告管理")
    public Result watchAdvertisement(
            @ApiParam(value = "广告ID", required = true) @RequestParam String adId,
            @ApiParam(value = "司机ID", required = true) @RequestParam String driverId) {
        return additionService.watchAdvertisement(adId, driverId);
    }

    @PostMapping("/ad/add")
    @Operation(summary = "添加广告", description = "添加一个新的广告", tags = "商品管理API - 广告管理")
    public Result addAd(@RequestBody @Parameter(description = "广告信息") Advertisement adInfo) {
        return additionService.addAd(adInfo);
    }

    @PutMapping("/ad/update")
    @Operation(summary = "更新广告", description = "更新现有的广告信息", tags = "商品管理API - 广告管理")
    public Result updateAd(@RequestBody @Parameter(description = "广告信息") Advertisement adInfo) {
        return additionService.updateAd(adInfo);
    }

    @DeleteMapping("/ad/{adId}")
    @Operation(summary = "删除广告", description = "根据广告ID删除广告", tags = "商品管理API - 广告管理")
    public Result deleteAd(@Parameter(description = "广告ID") @PathVariable String adId) {
        return additionService.deleteAd(adId);
    }

//    @PostMapping("/ad/playback")
//    @Operation(summary = "记录广告播放", description = "记录特定司机播放的广告", tags = "商品管理API - 广告管理")
//    public Result recordAdPlayback(
//            @Parameter(description = "广告ID") @RequestParam String adId,
//            @Parameter(description = "司机ID") @RequestParam String driverId) {
//        return additionService.recordAdPlayback(adId, driverId);
//    }




}

