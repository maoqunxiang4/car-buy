/*
 Navicat Premium Data Transfer

 Source Server         : randomBuy
 Source Server Type    : MySQL
 Source Server Version : 50740
 Source Host           : kin.qinsy.ltd:3306
 Source Schema         : randomBuy

 Target Server Type    : MySQL
 Target Server Version : 50740
 File Encoding         : 65001

 Date: 08/10/2023 23:00:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ad_total_playback_record
-- ----------------------------
DROP TABLE IF EXISTS `ad_total_playback_record`;
CREATE TABLE `ad_total_playback_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addition_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_number` double(255, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ad_total_playback_record
-- ----------------------------
INSERT INTO `ad_total_playback_record` VALUES ('1696183895843942400', 'ad1', 3);

-- ----------------------------
-- Table structure for advertisement
-- ----------------------------
DROP TABLE IF EXISTS `advertisement`;
CREATE TABLE `advertisement`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `vertiser` double(255, 0) NULL DEFAULT NULL,
  `start_date` datetime(0) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `target_audience` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of advertisement
-- ----------------------------
INSERT INTO `advertisement` VALUES ('ad1', 'Summer Sale', 'Up to 50% off on selected items!', 1000, '2023-06-01 00:00:00', '2023-08-31 00:00:00', 'All Users');
INSERT INTO `advertisement` VALUES ('ad2', 'Winter Collection', 'New arrivals for the winter season!', 1500, '2023-12-01 00:00:00', '2024-02-28 00:00:00', 'All Users');
INSERT INTO `advertisement` VALUES ('ad3', 'Back to School', 'Get ready for school with our latest collection!', 800, '2023-08-01 00:00:00', '2023-09-30 00:00:00', 'Students');
INSERT INTO `advertisement` VALUES ('ad4', 'Summer Sale', 'Up to 50% off on summer collection!', 2000, '2023-06-01 00:00:00', '2023-08-31 00:00:00', 'All Users');
INSERT INTO `advertisement` VALUES ('ad5', 'Tech Fest', 'Latest gadgets at unbeatable prices!', 2500, '2023-09-01 00:00:00', '2023-10-31 00:00:00', 'Tech Enthusiasts');
INSERT INTO `advertisement` VALUES ('ad6', 'Fitness First', 'Exclusive deals on fitness gear!', 1200, '2023-11-01 00:00:00', '2023-12-31 00:00:00', 'Fitness Enthusiasts');
INSERT INTO `advertisement` VALUES ('ad7', 'Travel Deals', 'Amazing discounts on travel packages!', 3000, '2024-01-01 00:00:00', '2024-02-28 00:00:00', 'Travelers');
INSERT INTO `advertisement` VALUES ('ad8', 'Spring Collection', 'Fresh arrivals for the spring season!', 1700, '2024-03-01 00:00:00', '2024-05-31 00:00:00', 'All Users');

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `discount_amount` double NOT NULL,
  `minimum_purchase` double NULL DEFAULT NULL,
  `start_date` datetime(0) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `status` enum('ACTIVE','EXPIRED','USED') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'ACTIVE',
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of coupon
-- ----------------------------
INSERT INTO `coupon` VALUES ('1696539840822259712', '2', '2', 1, 1, '2023-08-30 00:00:00', '2023-08-30 00:00:00', 'ACTIVE', '2023-08-29 00:00:00', '2023-08-29 00:00:00');

-- ----------------------------
-- Table structure for daily_driver_ad_playback_record
-- ----------------------------
DROP TABLE IF EXISTS `daily_driver_ad_playback_record`;
CREATE TABLE `daily_driver_ad_playback_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  `ad_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `driver_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `number_of_views` double(255, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of daily_driver_ad_playback_record
-- ----------------------------
INSERT INTO `daily_driver_ad_playback_record` VALUES ('1696183896183681024', '2023-08-28 00:00:00', 'ad1', 'driver1', 3);

-- ----------------------------
-- Table structure for daily_sales_record
-- ----------------------------
DROP TABLE IF EXISTS `daily_sales_record`;
CREATE TABLE `daily_sales_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `daily_sales_amount` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of daily_sales_record
-- ----------------------------
INSERT INTO `daily_sales_record` VALUES ('1696158723300417537', '2023-08-28 00:00:00', 'product1', 10000);

-- ----------------------------
-- Table structure for daily_views_record
-- ----------------------------
DROP TABLE IF EXISTS `daily_views_record`;
CREATE TABLE `daily_views_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  `ad_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `daily_ad_views` double(11, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of daily_views_record
-- ----------------------------

-- ----------------------------
-- Table structure for device
-- ----------------------------
DROP TABLE IF EXISTS `device`;
CREATE TABLE `device`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `driver_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_check` date NULL DEFAULT NULL,
  `model` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO `device` VALUES ('driver1', 'driver1', '2', '2023-08-29', '2');

-- ----------------------------
-- Table structure for driver
-- ----------------------------
DROP TABLE IF EXISTS `driver`;
CREATE TABLE `driver`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `register_date` date NULL DEFAULT NULL,
  `last_login` date NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 0,
  `vehicle_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of driver
-- ----------------------------
INSERT INTO `driver` VALUES ('driver1', '1234567890', NULL, NULL, NULL, '2023-01-01', '2023-05-01', 0, 'vehicle1');
INSERT INTO `driver` VALUES ('driver2', '1122334455', NULL, NULL, NULL, '2023-02-01', '2023-05-15', 0, 'vehicle2');
INSERT INTO `driver` VALUES ('driver3', '5566778899', NULL, NULL, NULL, '2023-03-01', '2023-05-20', 0, 'vehicle3');
INSERT INTO `driver` VALUES ('driver4', '9988776655', NULL, NULL, NULL, '2023-04-01', '2023-05-25', 0, 'vehicle4');
INSERT INTO `driver` VALUES ('driver5', '7766554433', NULL, NULL, NULL, '2023-05-01', '2023-06-05', 0, 'vehicle5');
INSERT INTO `driver` VALUES ('driver6', '6655443322', NULL, NULL, NULL, '2023-06-01', '2023-06-20', 0, 'vehicle6');
INSERT INTO `driver` VALUES ('driver7', '5544332211', NULL, NULL, NULL, '2023-07-01', '2023-07-15', 0, 'vehicle7');
INSERT INTO `driver` VALUES ('driver8', '4433221100', NULL, NULL, NULL, '2023-08-01', '2023-08-25', 0, 'vehicle8');

-- ----------------------------
-- Table structure for driver_advertisement
-- ----------------------------
DROP TABLE IF EXISTS `driver_advertisement`;
CREATE TABLE `driver_advertisement`  (
  `driver_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `advertisement_id` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of driver_advertisement
-- ----------------------------
INSERT INTO `driver_advertisement` VALUES ('driver1', 'ad1');
INSERT INTO `driver_advertisement` VALUES ('driver1', 'ad2');

-- ----------------------------
-- Table structure for driver_order
-- ----------------------------
DROP TABLE IF EXISTS `driver_order`;
CREATE TABLE `driver_order`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `passenger_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `driver_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_date` date NULL DEFAULT NULL,
  `amount` double(10, 2) NULL DEFAULT NULL,
  `vehicle_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` double(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of driver_order
-- ----------------------------
INSERT INTO `driver_order` VALUES ('1696158721719672832', 'product1', '1', 'driver1', '2023-08-30', 2.00, '1', 100.00);

-- ----------------------------
-- Table structure for driver_product
-- ----------------------------
DROP TABLE IF EXISTS `driver_product`;
CREATE TABLE `driver_product`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `stock` int(11) NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `driver_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_id`(`id`) USING BTREE,
  CONSTRAINT `fk_product` FOREIGN KEY (`id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of driver_product
-- ----------------------------
INSERT INTO `driver_product` VALUES ('ssp1', 'Sample Product', 'This is a sample product.', 50.00, 100, 'https://sample.com/image.jpg', 'Electronics', 'driver1');
INSERT INTO `driver_product` VALUES ('ssp2', 'Sample Product 2', 'Another sample product.', 60.00, 80, 'https://sample.com/image2.jpg', 'Fashion', 'driver2');
INSERT INTO `driver_product` VALUES ('ssp3', 'Sample Product 3', 'Yet another sample product.', 70.00, 90, 'https://sample.com/image3.jpg', 'Books', 'driver3');

-- ----------------------------
-- Table structure for passenger
-- ----------------------------
DROP TABLE IF EXISTS `passenger`;
CREATE TABLE `passenger`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `register_date` date NULL DEFAULT NULL,
  `last_login` date NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of passenger
-- ----------------------------
INSERT INTO `passenger` VALUES ('passenger1', '0987654321', NULL, NULL, NULL, '2023-01-10', '2023-05-10', 0);
INSERT INTO `passenger` VALUES ('passenger2', '8899776655', NULL, NULL, NULL, '2023-02-10', '2023-05-18', 0);
INSERT INTO `passenger` VALUES ('passenger3', '6677889900', NULL, NULL, NULL, '2023-03-10', '2023-05-22', 0);

-- ----------------------------
-- Table structure for platform_supervisor
-- ----------------------------
DROP TABLE IF EXISTS `platform_supervisor`;
CREATE TABLE `platform_supervisor`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `register_date` date NULL DEFAULT NULL,
  `last_login` date NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_supervisor
-- ----------------------------
INSERT INTO `platform_supervisor` VALUES ('1697131929318469633', NULL, '2023-08-31', '2023-08-31', 0);
INSERT INTO `platform_supervisor` VALUES ('1697132270202142722', NULL, '2023-08-31', '2023-08-31', 0);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `price` decimal(10, 2) NOT NULL,
  `image_url` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` date NULL DEFAULT NULL,
  `updated_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('ssp1', 'Sample Product', 'This is a sample product for supply station.', 55.00, 'https://sample.com/station_image.jpg', 'Electronics', '2023-05-01', '2023-05-01');
INSERT INTO `product` VALUES ('ssp2', 'Sample Product 2', 'Another sample product for supply station.', 65.00, 'https://sample.com/station_image2.jpg', 'Fashion', '2023-05-10', '2023-05-10');
INSERT INTO `product` VALUES ('ssp3', 'Sample Product 3', 'Yet another sample product for supply station.', 75.00, 'https://sample.com/station_image3.jpg', 'Books', '2023-05-15', '2023-05-15');

-- ----------------------------
-- Table structure for product_total_sales_record
-- ----------------------------
DROP TABLE IF EXISTS `product_total_sales_record`;
CREATE TABLE `product_total_sales_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addition_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_number` double(255, 0) NULL DEFAULT NULL,
  `total_revenue` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `average_price` double(255, 0) NULL DEFAULT NULL,
  `img_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product_total_sales_record
-- ----------------------------
INSERT INTO `product_total_sales_record` VALUES ('product1', 'product1', 0, '10000.0', 10000, 'https://sample.com/image.jpg');

-- ----------------------------
-- Table structure for sales_record
-- ----------------------------
DROP TABLE IF EXISTS `sales_record`;
CREATE TABLE `sales_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `x` double NULL DEFAULT NULL,
  `y` double NULL DEFAULT NULL,
  `driver_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `passenger_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sale_date` date NULL DEFAULT NULL,
  `sale_price` double(255, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_record
-- ----------------------------
INSERT INTO `sales_record` VALUES ('1696158721719537664', 1, 1, 'driver1', '1', '2023-08-28', 10000);

-- ----------------------------
-- Table structure for station_staff
-- ----------------------------
DROP TABLE IF EXISTS `station_staff`;
CREATE TABLE `station_staff`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `mobile` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `register_date` datetime(0) NULL DEFAULT NULL,
  `last_login` datetime(0) NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT 0,
  `station_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of station_staff
-- ----------------------------
INSERT INTO `station_staff` VALUES ('driver1', '2', '2', '2', '2', '2023-08-30 00:00:00', '2023-08-30 00:00:00', 0, 'station1', '2');

-- ----------------------------
-- Table structure for supply_station
-- ----------------------------
DROP TABLE IF EXISTS `supply_station`;
CREATE TABLE `supply_station`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `station_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `contact` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `operating_hours` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `x` decimal(10, 6) NULL DEFAULT NULL,
  `y` decimal(10, 6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supply_station
-- ----------------------------
INSERT INTO `supply_station` VALUES ('station1', '1', '1', '1', '1', 1.000000, 1.000000);
INSERT INTO `supply_station` VALUES ('station2', NULL, 'Jane Smith', '10:00 AM - 6:00 PM', 'Another Street', 34.052300, -118.243800);
INSERT INTO `supply_station` VALUES ('station3', NULL, 'Robert Brown', '11:00 AM - 7:00 PM', 'Third Street', 34.052400, -118.243900);
INSERT INTO `supply_station` VALUES ('station4', NULL, 'Alice Green', '9:00 AM - 5:00 PM', 'Fourth Street', 34.052500, -118.244000);
INSERT INTO `supply_station` VALUES ('station5', NULL, 'John White', '10:00 AM - 7:00 PM', 'Fifth Street', 34.052600, -118.244100);
INSERT INTO `supply_station` VALUES ('station6', NULL, 'Lucy Black', '11:00 AM - 8:00 PM', 'Sixth Street', 34.052700, -118.244200);
INSERT INTO `supply_station` VALUES ('station7', NULL, 'Mike Yellow', '12:00 PM - 9:00 PM', 'Seventh Street', 34.052800, -118.244300);
INSERT INTO `supply_station` VALUES ('station8', NULL, 'Nina Blue', '1:00 PM - 10:00 PM', 'Eighth Street', 34.052900, -118.244400);

-- ----------------------------
-- Table structure for supply_station_daily_sales_record
-- ----------------------------
DROP TABLE IF EXISTS `supply_station_daily_sales_record`;
CREATE TABLE `supply_station_daily_sales_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date` date NULL DEFAULT NULL,
  `station_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_value` double(255, 0) NULL DEFAULT NULL,
  `daily_revenue` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supply_station_daily_sales_record
-- ----------------------------
INSERT INTO `supply_station_daily_sales_record` VALUES ('1', '2022-07-22', 'station1', 'ssp1', 100, 1000);

-- ----------------------------
-- Table structure for supply_station_order
-- ----------------------------
DROP TABLE IF EXISTS `supply_station_order`;
CREATE TABLE `supply_station_order`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `product_number` int(11) NOT NULL,
  `order_date` datetime(0) NOT NULL,
  `amount` double NOT NULL,
  `supply_station_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supply_station_order
-- ----------------------------
INSERT INTO `supply_station_order` VALUES ('1', 'ssp1', 1, '2023-08-30 23:29:29', 100, 'station1');

-- ----------------------------
-- Table structure for supply_station_store
-- ----------------------------
DROP TABLE IF EXISTS `supply_station_store`;
CREATE TABLE `supply_station_store`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `supply_station_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `stock_quantity` bigint(11) NULL DEFAULT NULL,
  `last_updated` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `supply_station_id`(`supply_station_id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `supply_station_store_ibfk_1` FOREIGN KEY (`supply_station_id`) REFERENCES `supply_station` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `supply_station_store_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supply_station_store
-- ----------------------------
INSERT INTO `supply_station_store` VALUES ('1', 'station1', 'ssp1', 300, '2023-08-30');

-- ----------------------------
-- Table structure for supply_station_total_sales_record
-- ----------------------------
DROP TABLE IF EXISTS `supply_station_total_sales_record`;
CREATE TABLE `supply_station_total_sales_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addition_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_number` double(255, 0) NULL DEFAULT NULL,
  `station_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_revenue` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of supply_station_total_sales_record
-- ----------------------------

-- ----------------------------
-- Table structure for user_behavior_record
-- ----------------------------
DROP TABLE IF EXISTS `user_behavior_record`;
CREATE TABLE `user_behavior_record`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `x` double NULL DEFAULT NULL,
  `y` double NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `behavior_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `behavior_date` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_behavior_record
-- ----------------------------
INSERT INTO `user_behavior_record` VALUES ('1696158721719537665', 1, 1, '1', 'product1', 'purchase', '2023-08-28');

-- ----------------------------
-- Table structure for user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `user_coupon`;
CREATE TABLE `user_coupon`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `coupon_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'CLAIMED',
  `claimed_at` date NULL DEFAULT NULL,
  `used_at` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_coupon
-- ----------------------------
INSERT INTO `user_coupon` VALUES ('uc1', 'passenger1', 'coupon1', 'CLAIMED', '2023-05-10', NULL);
INSERT INTO `user_coupon` VALUES ('uc2', 'passenger2', 'coupon2', 'CLAIMED', '2023-06-20', NULL);
INSERT INTO `user_coupon` VALUES ('uc3', 'passenger3', 'coupon3', 'CLAIMED', '2023-06-21', NULL);

SET FOREIGN_KEY_CHECKS = 1;
